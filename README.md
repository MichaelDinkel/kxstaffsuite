# kxStaffSuite
kxStaffSuite is a plugin for Spigot and BungeeCord intended to act as a server's moderation suite.

  - Supports temporary and permanent bans and mutes.
  - Saves punishment history for repeat offenders.
  - Has the option to poison an IP address to stealtily catch those with tons of alts.
  - Private chats for staff members.
  - Can silence the entire server's chat if things are getting out of control.

### Installation

kxStaffSuite is very simply to install. Just drop the plugin jar file into the plugins folder of your Spigot server or your BungeeCord proxy (but not both).
Once installed, start your server so kxStaffSuite creates its configuration file. It will work out of the box, but if you want to use MySQL, you need to configure the plugin so it can connect to your database.

kxStaffSuite requires Spigot 1.13 or later to run.

### Permissions

kxStaffSuite has a simple permission system.

All commands except staff chats have the permission kxstaffsuite.command.<name> where the name is the name of the command.

Staff chats have the permission kxstaffsuite.command.staffchat.<name> where the name is the name you have specified in the configuration.

Notifications have the format kxstaffsuite.notify.<name> where the name is the name of the command used.

Wildcards are supported on all permissions.

### Configuration

kxStaffSuite has a short and simple configuration.
  - staffChats: Here you can define as many staff chats as you please. The color characters can be found [here](https://minecraft.gamepedia.com/Formatting_codes#Color_codes). Possible permission defaults are "true", "false", "op", and "not op".
  - useVanillaBans: If this is enabled, kxStaffSuite will write bans to the vanilla ban list so they persist even if the plugin fails to load for any reason. This option does nothing on BungeeCord.
  - sql: The settings for connecting to a MySQL database.

### License

kxStaffSuite is licenced under the MIT license. [Click here](LICENSE.md) to view the license.