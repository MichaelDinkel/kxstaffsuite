package com.kaixeleron.staffsuite.bukkit;

import com.kaixeleron.staffsuite.bukkit.command.*;
import com.kaixeleron.staffsuite.bukkit.database.Database;
import com.kaixeleron.staffsuite.bukkit.database.DatabaseException;
import com.kaixeleron.staffsuite.bukkit.database.SQLDatabase;
import com.kaixeleron.staffsuite.bukkit.database.YAMLDatabase;
import com.kaixeleron.staffsuite.bukkit.external.DiscouragementHandler;
import com.kaixeleron.staffsuite.bukkit.listener.ChatListener;
import com.kaixeleron.staffsuite.bukkit.listener.DiscouragementListener;
import com.kaixeleron.staffsuite.bukkit.listener.LoginListener;
import com.kaixeleron.staffsuite.time.TimeManager;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandMap;
import org.bukkit.command.PluginCommand;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;

public class SuiteMain extends JavaPlugin {

    private Database database = null;

    @SuppressWarnings("ConstantConditions")
    @Override
    public void onEnable() {

        saveDefaultConfig();

        if (getConfig().getBoolean("sql.enabled")) {

            try {

                ConfigurationSection c = getConfig().getConfigurationSection("sql");

                database = new SQLDatabase(c.getString("host"), c.getInt("port"), c.getString("database"), c.getString("username"), c.getString("password"));

            } catch (SQLException e) {

                System.err.println("Could not connect to the MySQL database.");
                e.printStackTrace();

            }

        } else {

            try {

                database = new YAMLDatabase(this);

            } catch (DatabaseException e) {

                System.err.println("Could not set up the YAML database.");
                e.printStackTrace();

            }

        }

        TimeManager tm = new TimeManager();

        ChatListener chatListener = new ChatListener(database, tm);

        getCommand("kick").setExecutor(new CommandKick());
        getCommand("silence").setExecutor(new CommandSilence(chatListener));

        DiscouragementHandler handler = null;

        try {

            Class.forName("com.kaixeleron.packethook.PacketManager");

            handler = new DiscouragementHandler(this, database);

            getServer().getPluginManager().registerEvents(new DiscouragementListener(handler), this);

        } catch (ClassNotFoundException e) {

            System.out.println("kxPacketHook is not installed. User discouragement will not work.");

        }

        if (database == null) {

            DatabaseFailureNotifier notifier = new DatabaseFailureNotifier();

            getCommand("tempmute").setExecutor(notifier);
            getCommand("mute").setExecutor(notifier);
            getCommand("tempban").setExecutor(notifier);
            getCommand("ban").setExecutor(notifier);
            getCommand("unmute").setExecutor(notifier);
            getCommand("unban").setExecutor(notifier);
            getCommand("check").setExecutor(notifier);
            getCommand("banhistory").setExecutor(notifier);
            getCommand("mutehistory").setExecutor(notifier);
            getCommand("alts").setExecutor(notifier);
            getCommand("poison").setExecutor(notifier);
            getCommand("unpoison").setExecutor(notifier);
            getCommand("checkpoison").setExecutor(notifier);
            getCommand("discourage").setExecutor(notifier);
            getCommand("undiscourage").setExecutor(notifier);

        } else {

            getCommand("tempmute").setExecutor(new CommandTempmute(database, tm));
            getCommand("tempban").setExecutor(new CommandTempban(this, database, tm));
            getCommand("mute").setExecutor(new CommandMute(database));
            getCommand("ban").setExecutor(new CommandBan(this, database));
            getCommand("unmute").setExecutor(new CommandUnmute(database));
            getCommand("unban").setExecutor(new CommandUnban(this, database));
            getCommand("check").setExecutor(new CommandCheck(database, tm));
            getCommand("banhistory").setExecutor(new CommandBanHistory(database, tm));
            getCommand("mutehistory").setExecutor(new CommandMuteHistory(database, tm));
            getCommand("alts").setExecutor(new CommandAlts(database));
            getCommand("poison").setExecutor(new CommandPoison(database));
            getCommand("unpoison").setExecutor(new CommandUnpoison(database));
            getCommand("checkpoison").setExecutor(new CommandCheckPoison(database, tm));
            getCommand("discourage").setExecutor(new CommandDiscourage(database, handler));
            getCommand("undiscourage").setExecutor(new CommandUndiscourage(database, handler));

        }

        getServer().getPluginManager().registerEvents(new LoginListener(database, tm), this);
        getServer().getPluginManager().registerEvents(chatListener, this);

        try {

            Constructor<PluginCommand> constructor = PluginCommand.class.getDeclaredConstructor(String.class, Plugin.class);
            constructor.setAccessible(true);

            Field f = getServer().getPluginManager().getClass().getDeclaredField("commandMap");
            f.setAccessible(true);

            CommandMap map = (CommandMap) f.get(getServer().getPluginManager());

            Permission star = getServer().getPluginManager().getPermission("kxstaffsuite.command.staffchat.*");

            for (String s : getConfig().getConfigurationSection("staffChats").getKeys(false)) {

                Permission p = new Permission(String.format("kxstaffsuite.command.staffchat.%s", s));
                p.addParent(star, true);

                switch (getConfig().getString(String.format("staffChats.%s", s)).toLowerCase()) {

                    case "true":

                        p.setDefault(PermissionDefault.TRUE);

                        break;

                    case "false":

                        p.setDefault(PermissionDefault.FALSE);

                        break;

                    case "not op":
                    case "not_op":
                    case "not-op":

                        p.setDefault(PermissionDefault.NOT_OP);

                        break;

                    case "op":
                    default:

                        p.setDefault(PermissionDefault.OP);

                        break;

                }

                getServer().getPluginManager().addPermission(p);

                PluginCommand cmd = constructor.newInstance(s, this);
                cmd.setDescription("Staff chat");
                cmd.setPermission(String.format("kxstaffsuite.command.staffchat.%s", s));
                cmd.setPermissionMessage(ChatColor.RED + "Access denied.");
                cmd.setExecutor(new CommandStaffChat(ChatColor.getByChar(getConfig().getString(String.format("staffChats.%s.color", s))), s));

                map.register(s, cmd);

            }

        } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException | NoSuchFieldException e) {

            e.printStackTrace();

        }

    }

    @Override
    public void onDisable() {

        try {

            database.close();

        } catch (DatabaseException e) {

            e.printStackTrace();

        }

    }
}
