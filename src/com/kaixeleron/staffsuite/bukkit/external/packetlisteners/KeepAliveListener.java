package com.kaixeleron.staffsuite.bukkit.external.packetlisteners;

import com.kaixeleron.packethook.event.PacketInListener;
import com.kaixeleron.packethook.packet.WrappedPacketIn;
import com.kaixeleron.packethook.packet.in.WrappedPacketPlayInKeepAlive;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class KeepAliveListener implements PacketInListener {

    private final List<Player> toDrop;

    public KeepAliveListener() {

        toDrop = new ArrayList<>();

    }

    @Override
    public void handlePacket(WrappedPacketIn packet) {

        if (packet instanceof WrappedPacketPlayInKeepAlive && toDrop.contains(packet.getPlayer())) {

            packet.setCancelled(true);

        }

    }

    public void drop(Player player) {

        toDrop.add(player);

    }

    public void clear(Player player) {

        toDrop.remove(player);

    }

}
