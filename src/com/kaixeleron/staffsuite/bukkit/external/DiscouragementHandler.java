package com.kaixeleron.staffsuite.bukkit.external;

import com.kaixeleron.packethook.PacketManager;
import com.kaixeleron.packethook.packet.in.WrappedPacketPlayInKeepAlive;
import com.kaixeleron.staffsuite.bukkit.SuiteMain;
import com.kaixeleron.staffsuite.bukkit.database.Database;
import com.kaixeleron.staffsuite.bukkit.database.DatabaseException;
import com.kaixeleron.staffsuite.bukkit.external.packetlisteners.KeepAliveListener;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DiscouragementHandler {

    private final Database database;

    private final List<Player> discouraged;

    private static final int NUM_DISCOURAGEMENTS = 1;

    private final KeepAliveListener keepAliveListener;

    public DiscouragementHandler(final SuiteMain main, Database database) {

        this.database = database;
        this.discouraged = new ArrayList<>();

        final Random random = new Random();

        keepAliveListener = new KeepAliveListener();

        PacketManager.getInstance().registerInPacketListener(keepAliveListener, WrappedPacketPlayInKeepAlive.class);

        new BukkitRunnable() {

            @Override
            public void run() {

                new BukkitRunnable() {

                    @Override
                    public void run() {

                        for (Player player : discouraged) {

                            switch (random.nextInt(NUM_DISCOURAGEMENTS)) {

                                case 0:

                                    keepAliveListener.drop(player);
                                    break;

                            }

                        }

                    }

                }.runTaskLater(main, random.nextInt(100));

            }

        }.runTaskTimer(main, 300L, 300L);

    }

    public void addDiscouragement(Player player) {

        discouraged.add(player);

    }

    public void removeDiscouragement(Player player) {

        discouraged.remove(player);
        keepAliveListener.clear(player);

    }

    public void playerJoined(Player player) {

        try {

            if (database.isDiscouraged(player)) {

                discouraged.add(player);

            }

        } catch (DatabaseException ignored) {}

    }

    public void playerLeft(Player player) {

        discouraged.remove(player);
        keepAliveListener.clear(player);

    }

}
