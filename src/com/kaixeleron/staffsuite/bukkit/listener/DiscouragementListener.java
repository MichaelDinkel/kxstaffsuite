package com.kaixeleron.staffsuite.bukkit.listener;

import com.kaixeleron.staffsuite.bukkit.external.DiscouragementHandler;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class DiscouragementListener implements Listener {

    private final DiscouragementHandler handler;

    public DiscouragementListener(DiscouragementHandler handler) {

        this.handler = handler;

    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {

        handler.playerJoined(event.getPlayer());

    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {

        handler.playerLeft(event.getPlayer());

    }

    @EventHandler
    public void onQuit(PlayerKickEvent event) {

        handler.playerLeft(event.getPlayer());

    }

}
