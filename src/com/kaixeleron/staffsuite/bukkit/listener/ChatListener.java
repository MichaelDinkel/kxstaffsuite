package com.kaixeleron.staffsuite.bukkit.listener;

import com.kaixeleron.staffsuite.bukkit.database.DatabaseException;
import com.kaixeleron.staffsuite.bukkit.database.Database;
import com.kaixeleron.staffsuite.bukkit.database.data.Mute;
import com.kaixeleron.staffsuite.time.TimeManager;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatListener implements Listener {

    private final Database db;

    private final TimeManager timeManager;

    private volatile boolean silenced;

    public ChatListener(Database db, TimeManager timeManager) {

        this.db = db;

        this.timeManager = timeManager;

    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {

        if (silenced) {

            if (!event.getPlayer().hasPermission("kxstaffsuite.command.silence")) {

                event.setCancelled(true);
                event.getPlayer().sendMessage(ChatColor.RED + "The server is currently silenced.");

            }

        } else {

            try {

                Mute mute = db.getMute(event.getPlayer());

                if (mute != null) {

                    event.setCancelled(true);

                    long expiry = mute.getExpiry();

                    if (expiry == -1) {

                        event.getPlayer().sendMessage(new String[]{ChatColor.RED + "You are permanently muted.", ChatColor.RED + "Reason: " + ChatColor.RESET + mute.getReason()});

                    } else {

                        event.getPlayer().sendMessage(new String[]{ChatColor.RED + "You are temporarily muted.", ChatColor.RED + "Duration: " + ChatColor.RESET + timeManager.timeToString(expiry - System.currentTimeMillis()),
                                ChatColor.RED + "Reason: " + ChatColor.RESET + mute.getReason()});

                    }

                }

            } catch (DatabaseException e) {

                e.printStackTrace();

            }

        }

    }

    public void setSilenced(boolean silenced) {

        this.silenced = silenced;

    }

    public boolean isSilenced() {

        return silenced;

    }

}
