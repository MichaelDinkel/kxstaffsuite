package com.kaixeleron.staffsuite.bukkit.listener;

import com.kaixeleron.staffsuite.bukkit.database.data.Poison;
import com.kaixeleron.staffsuite.bukkit.database.data.Ban;
import com.kaixeleron.staffsuite.bukkit.database.Database;
import com.kaixeleron.staffsuite.bukkit.database.DatabaseException;
import com.kaixeleron.staffsuite.time.TimeManager;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

public class LoginListener implements Listener {

    private final Database db;

    private final TimeManager timeManager;

    public LoginListener(Database db, TimeManager timeManager) {

        this.db = db;

        this.timeManager = timeManager;

    }

    @EventHandler
    public void onLogin(PlayerLoginEvent event) {

        String ip = event.getAddress().toString().substring(1);

        if (ip.contains(":")) ip = ip.split("[%]")[0]; //IPv6

        try {

            db.updateAlt(ip, event.getPlayer().getUniqueId(), event.getPlayer().getName());

            Poison poison = db.getPoison(ip);

            Ban ban = db.getBan(event.getPlayer());

            if (poison != null && ban == null) {

                db.ban(event.getPlayer(), -1L, poison.getReason(), poison.getBy());

                event.disallow(PlayerLoginEvent.Result.KICK_BANNED, ChatColor.RED + "Banned\n\nReason: " + ChatColor.RESET + poison.getReason());

            } else {

                if (ban != null) {

                    long expiry = ban.getExpiry();

                    if (expiry == -1) {

                        event.disallow(PlayerLoginEvent.Result.KICK_BANNED, ChatColor.RED + "Banned\n\nReason: " + ChatColor.RESET + ban.getReason());

                    } else {

                        event.disallow(PlayerLoginEvent.Result.KICK_BANNED, ChatColor.RED + "Temporarily banned for " + ChatColor.RESET + timeManager.timeToString(expiry
                                - System.currentTimeMillis()) + ChatColor.RED + "\n\nReason: " + ChatColor.RESET + ban.getReason());

                    }

                }

            }

        } catch (DatabaseException e) {

            event.disallow(PlayerLoginEvent.Result.KICK_OTHER, "A database error has occurred while logging in. This does not affect any ban status you may have.");
            e.printStackTrace();

        }

    }

}
