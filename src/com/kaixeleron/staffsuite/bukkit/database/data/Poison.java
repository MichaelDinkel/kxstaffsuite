package com.kaixeleron.staffsuite.bukkit.database.data;

public class Poison {

    private final String ip, reason, by, expiredBy;

    private final long placed;

    private final boolean expired;

    public Poison(String ip, String reason, String by, boolean expired, String expiredby, long placed) {

        this.ip = ip;

        this.reason = reason;

        this.by = by;

        this.expired = expired;

        this.expiredBy = expiredby;

        this.placed = placed;

    }

    public Poison(String ip, String reason, String by) {

        this(ip, reason, by, false, null, -1L);

    }

    public Poison(String ip, String by) {

        this(ip, null, by);

    }

    public String getIp() {
        return ip;
    }

    public String getReason() {
        return reason;
    }

    public String getBy() {
        return by;
    }

    public boolean isExpired() {
        return expired;
    }

    public String getExpiredBy() {
        return expiredBy;
    }

    public long getPlaced() {
        return placed;
    }

}
