package com.kaixeleron.staffsuite.bukkit.database.data;

import java.util.UUID;

public class Discouragement {

    private final UUID id;

    private final String by, expiredBy;

    private final long placed;

    private final boolean expired;

    public Discouragement(UUID id, String by, boolean expired, String expiredby, long placed) {

        this.id = id;

        this.by = by;

        this.expired = expired;

        this.expiredBy = expiredby;

        this.placed = placed;

    }

    public Discouragement(UUID id, String by) {

        this(id, by, false, null, -1L);

    }

    public UUID getUserId() {
        return id;
    }

    public String getBy() {
        return by;
    }

    public boolean isExpired() {
        return expired;
    }

    public String getExpiredBy() {
        return expiredBy;
    }

    public long getPlaced() {
        return placed;
    }

}
