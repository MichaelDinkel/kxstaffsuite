package com.kaixeleron.staffsuite.bukkit.database.data;

import java.util.UUID;

public class Mute {

    private final UUID id;

    private final String reason, by, expiredBy;

    private final long expiry, placed;

    private final boolean expired;

    public Mute(UUID id, String reason, long expiry, String by, boolean expired, String expiredby, long placed) {

        this.id = id;

        this.reason = reason;

        this.expiry = expiry;

        this.by = by;

        this.expired = expired;

        this.expiredBy = expiredby;

        this.placed = placed;

    }

    public Mute(UUID id, String reason, long expiry, String by) {

        this(id, reason, expiry, by, false, null, -1L);

    }

    public Mute(UUID id, String by) {

        this(id, null, -2L, by);

    }

    public UUID getUserId() {
        return id;
    }

    public String getReason() {
        return reason;
    }

    public long getExpiry() {
        return expiry;
    }

    public String getBy() {
        return by;
    }

    public boolean isExpired() {
        return expired;
    }

    public String getExpiredBy() {
        return expiredBy;
    }

    public long getPlaced() {
        return placed;
    }

}
