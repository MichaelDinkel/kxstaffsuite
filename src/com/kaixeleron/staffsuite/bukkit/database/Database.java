package com.kaixeleron.staffsuite.bukkit.database;

import com.kaixeleron.staffsuite.bukkit.database.data.*;
import org.bukkit.OfflinePlayer;

import javax.annotation.Nullable;
import java.util.UUID;

public interface Database {

    void ban(OfflinePlayer p, long expiry, String reason, String by) throws DatabaseException;

    void mute(OfflinePlayer p, long expiry, String reason, String by) throws DatabaseException;

    void poison(String ip, String reason, String by) throws DatabaseException;

    void unban(OfflinePlayer p, String by) throws DatabaseException;

    void unmute(OfflinePlayer p, String by) throws DatabaseException;

    void unpoison(String ip, String by) throws DatabaseException;

    void updateAlt(String ip, UUID uuid, String name) throws DatabaseException;

    void discourage(OfflinePlayer player, String by) throws DatabaseException;

    void undiscourage(OfflinePlayer player, String by) throws DatabaseException;

    @Nullable Ban getBan(OfflinePlayer p) throws DatabaseException;

    @Nullable Mute getMute(OfflinePlayer p) throws DatabaseException;

    @Nullable Poison getPoison(String ip) throws DatabaseException;

    Ban[] getBanHistory(OfflinePlayer p) throws DatabaseException;

    Mute[] getMuteHistory(OfflinePlayer p) throws DatabaseException;

    Alt[] getAlts(String ip) throws DatabaseException;

    Alt[] getAlts(UUID id) throws DatabaseException;

    Poison[] getPoisonHistory(String ip) throws DatabaseException;

    boolean isDiscouraged(OfflinePlayer player) throws DatabaseException;

    void close() throws DatabaseException;

}
