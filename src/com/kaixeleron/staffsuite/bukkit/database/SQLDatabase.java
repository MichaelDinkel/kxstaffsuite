package com.kaixeleron.staffsuite.bukkit.database;

import com.kaixeleron.staffsuite.bukkit.database.data.*;
import org.bukkit.OfflinePlayer;

import java.sql.*;
import java.util.*;

public class SQLDatabase implements Database {

    private final String hostname, database, username, password;

    private final int port;

    private final Map<UUID, Ban> banCache;
    private final Map<UUID, Mute> muteCache;
    private final Map<String, Poison> poisonCache;
    private final Map<UUID, Discouragement> discouragementCache;
    private final List<Alt> altCache;
    private final SQLThread thread;
    private Connection c = null;

    public SQLDatabase(String hostname, int port, String database, String username, String password) throws SQLException {

        this.hostname = hostname;
        this.port = port;
        this.database = database;
        this.username = username;
        this.password = password;

        banCache = new HashMap<>();
        muteCache = new HashMap<>();
        poisonCache = new HashMap<>();
        discouragementCache = new HashMap<>();

        altCache = new ArrayList<>();

        connect();

        c.prepareStatement("CREATE TABLE IF NOT EXISTS `kxstaffsuite_bans` (`uuid` CHAR(36), `expiry` BIGINT NULL DEFAULT 0, `reason` VARCHAR(256), `bannedby` VARCHAR(16), `expired` BIT, `expiredby` VARCHAR(16), `placed` BIGINT NULL DEFAULT 0);").executeUpdate();
        c.prepareStatement("CREATE TABLE IF NOT EXISTS `kxstaffsuite_mutes` (`uuid` CHAR(36), `expiry` BIGINT NULL DEFAULT 0, `reason` VARCHAR(256), `mutedby` VARCHAR(16), `expired` BIT, `expiredby` VARCHAR(16), `placed` BIGINT NULL DEFAULT 0);").executeUpdate();
        c.prepareStatement("CREATE TABLE IF NOT EXISTS `kxstaffsuite_alts` (`ip` VARCHAR(39), `uuid` CHAR(36), `name` VARCHAR(16), `lastlogin` BIGINT NULL DEFAULT 0, UNIQUE (`uuid`));").executeUpdate();
        c.prepareStatement("CREATE TABLE IF NOT EXISTS `kxstaffsuite_poison` (`ip` VARCHAR(39), `reason` VARCHAR(256), `poisonedby` VARCHAR(16), `removed` BIT, `removedby` VARCHAR(16), `placed` BIGINT NULL DEFAULT 0);").executeUpdate();
        c.prepareStatement("CREATE TABLE IF NOT EXISTS `kxstaffsuite_discouraged` (`uuid` CHAR(36), `discouragedby` VARCHAR(16), `removed` BIT, `removedby` VARCHAR(16), `placed` BIGINT NULL DEFAULT 0);").executeUpdate();

        thread = new SQLThread(hostname, port, database, username, password);
        thread.start();

    }

    private void connect() throws SQLException {

        if (c == null || c.isClosed()) c = DriverManager.getConnection(String.format("jdbc:mysql://%s:%d/%s", hostname, port, database), username, password);

    }

    private void closeSilently(AutoCloseable... ac) {

        for (AutoCloseable a : ac) {

            try {

                a.close();

            } catch (Exception ignored) {}

        }

    }

    @Override
    public void ban(OfflinePlayer p, long expiry, String reason, String by) {

        Ban b = new Ban(p.getUniqueId(), reason, expiry, by);

        banCache.put(p.getUniqueId(), b);

        thread.addBan(b);

    }

    @Override
    public void mute(OfflinePlayer p, long expiry, String reason, String by) {

        Mute m = new Mute(p.getUniqueId(), reason, expiry, by);

        muteCache.put(p.getUniqueId(), m);

        thread.addMute(m);

    }

    @Override
    public void poison(String ip, String reason, String by) {

        Poison p = new Poison(ip, reason, by);

        poisonCache.put(ip, p);

        thread.addPoison(p);

    }

    @Override
    public void unban(OfflinePlayer p, String by) {

        Ban b = new Ban(p.getUniqueId(), by);

        banCache.remove(p.getUniqueId());

        thread.removeBan(b);

    }

    @Override
    public void unmute(OfflinePlayer p, String by) {

        Mute m = new Mute(p.getUniqueId(), by);

        muteCache.remove(p.getUniqueId());

        thread.removeMute(m);

    }

    @Override
    public void unpoison(String ip, String by) {

        Poison p = new Poison(ip, by);

        poisonCache.remove(ip);

        thread.removePoison(p);

    }

    @Override
    public void updateAlt(String ip, UUID uuid, String name) {

        Alt a = new Alt(ip, uuid, name, System.currentTimeMillis());

        altCache.add(a);

        thread.addAlt(a);

    }

    @Override
    public void discourage(OfflinePlayer player, String by) {

        Discouragement discouragement = new Discouragement(player.getUniqueId(), by);

        discouragementCache.put(player.getUniqueId(), discouragement);
        thread.addDiscouragement(discouragement);

    }

    @Override
    public void undiscourage(OfflinePlayer player, String by) {

        Discouragement discouragement = new Discouragement(player.getUniqueId(), by);

        discouragementCache.remove(player.getUniqueId());
        thread.removeDiscouragement(discouragement);

    }

    @Override
    public Ban getBan(OfflinePlayer p) throws DatabaseException {

        if (banCache.containsKey(p.getUniqueId())) {

            Ban ban = banCache.get(p.getUniqueId());

            if (ban.getExpiry() != -1 && ban.getExpiry() < System.currentTimeMillis()) {

                unban(p, "-EXPIRED-");

                return null;

            } else {

                return ban;

            }

        }

        Ban ban = null;

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            connect();

            get = c.prepareStatement("SELECT `reason`,`expiry`,`bannedby` FROM `kxstaffsuite_bans` WHERE `uuid` = ? AND `expired` = ?;");
            get.setString(1, p.getUniqueId().toString());
            get.setBoolean(2, false);

            rs = get.executeQuery();

            if (rs.next()) {

                ban = new Ban(p.getUniqueId(), rs.getString("reason"), rs.getLong("expiry"), rs.getString("bannedby"));

            }

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(get, rs);

        }

        return ban;

    }

    @Override
    public Mute getMute(OfflinePlayer p) throws DatabaseException {

        if (muteCache.containsKey(p.getUniqueId())) {

            Mute mute = muteCache.get(p.getUniqueId());

            if (mute.getExpiry() != -1 && mute.getExpiry() < System.currentTimeMillis()) {

                unmute(p, "-EXPIRED-");

                return null;

            } else {

                return mute;

            }

        }

        Mute mute = null;

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            connect();

            get = c.prepareStatement("SELECT `reason`,`expiry`,`mutedby` FROM `kxstaffsuite_mutes` WHERE `uuid` = ? AND `expired` = ?;");
            get.setString(1, p.getUniqueId().toString());
            get.setBoolean(2, false);

            rs = get.executeQuery();

            if (rs.next()) {

                mute = new Mute(p.getUniqueId(), rs.getString("reason"), rs.getLong("expiry"), rs.getString("mutedby"));

            }

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(get, rs);

        }

        return mute;

    }

    @Override
    public Poison getPoison(String ip) throws DatabaseException {

        if (poisonCache.containsKey(ip)) return poisonCache.get(ip);

        Poison out = null;

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            connect();

            get = c.prepareStatement("SELECT `reason`, `poisonedby` FROM `kxstaffsuite_poison` WHERE `ip` = ? AND `removed` = ?;");
            get.setString(1, ip);
            get.setBoolean(2, false);
            rs = get.executeQuery();

            if (rs.next()) {

                out = new Poison(ip, rs.getString("reason"), rs.getString("poisonedby"));

            }

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(get, rs);

        }

        return out;

    }

    @Override
    public Ban[] getBanHistory(OfflinePlayer p) throws DatabaseException {

        Ban[] out = new Ban[0];

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            connect();

            get = c.prepareStatement("SELECT * FROM `kxstaffsuite_bans` WHERE `uuid` = ?", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            get.setString(1, p.getUniqueId().toString());

            rs = get.executeQuery();

            if (rs.last()) {

                out = new Ban[rs.getRow()];

                rs.beforeFirst();

                int current = 0;

                while (rs.next()) {

                    out[current] = new Ban(p.getUniqueId(), rs.getString("reason"), rs.getLong("expiry"),
                            rs.getString("bannedby"), rs.getBoolean("expired"), rs.getString("expiredby"),
                            rs.getLong("placed"));

                    current++;

                }

            }

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(get, rs);

        }

        return out;

    }

    @Override
    public Mute[] getMuteHistory(OfflinePlayer p) throws DatabaseException {

        Mute[] out = new Mute[0];

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            connect();

            get = c.prepareStatement("SELECT * FROM `kxstaffsuite_mutes` WHERE `uuid` = ?", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            get.setString(1, p.getUniqueId().toString());

            rs = get.executeQuery();

            if (rs.last()) {

                out = new Mute[rs.getRow()];

                rs.beforeFirst();

                int current = 0;

                while (rs.next()) {

                    out[current] = new Mute(p.getUniqueId(), rs.getString("reason"), rs.getLong("expiry"),
                            rs.getString("mutedby"), rs.getBoolean("expired"), rs.getString("expiredby"),
                            rs.getLong("placed"));

                    current++;

                }

            }

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(get, rs);

        }

        return out;

    }

    @Override
    public Alt[] getAlts(String ip) throws DatabaseException {

        Set<Alt> alts = new HashSet<>();

        for (Alt a : altCache) {

            if (a.getIp().equals(ip)) alts.add(a);

        }

        if (alts.size() > 0) return alts.toArray(new Alt[alts.size()]);

        Alt[] out = new Alt[0];

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            connect();

            get = c.prepareStatement("SELECT * FROM `kxstaffsuite_alts` WHERE `ip` = ?", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            get.setString(1, ip);

            rs = get.executeQuery();

            if (rs.last()) {

                out = new Alt[rs.getRow()];

                rs.beforeFirst();

                int current = 0;

                while (rs.next()) {

                    out[current] = new Alt(ip, UUID.fromString(rs.getString("uuid")), rs.getString("name"), rs.getLong("lastlogin"));

                    current++;

                }

            }

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(get, rs);

        }

        return out;

    }

    @Override
    public Alt[] getAlts(UUID id) throws DatabaseException {

        for (Alt a : altCache) {

            if (a.getId().equals(id)) {

                Set<Alt> alts = new HashSet<>();

                for (Alt alt : altCache) {

                    if (alt.getIp().equals(a.getIp())) alts.add(a);

                }

                return alts.toArray(new Alt[alts.size()]);

            }

        }

        Alt[] out = new Alt[0];

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            connect();

            get = c.prepareStatement("SELECT * FROM `kxstaffsuite_alts` WHERE `uuid` = ?", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            get.setString(1, id.toString());

            rs = get.executeQuery();

            if (rs.last()) {

                out = new Alt[rs.getRow()];

                rs.beforeFirst();

                int current = 0;

                while (rs.next()) {

                    out[current] = new Alt(rs.getString("ip"), id, rs.getString("name"), rs.getLong("lastlogin"));

                    current++;

                }

            }

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(get, rs);

        }

        return out;

    }

    @Override
    public Poison[] getPoisonHistory(String ip) throws DatabaseException {

        Poison[] out = new Poison[0];

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            connect();

            get = c.prepareStatement("SELECT * FROM `kxstaffsuite_poison` WHERE `ip` = ? AND `removed` = ?;", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            get.setString(1, ip);
            get.setBoolean(2, true);

            rs = get.executeQuery();

            if (rs.last()) {

                out = new Poison[rs.getRow()];

                rs.beforeFirst();

                int current = 0;

                while (rs.next()) {

                    out[current] = new Poison(ip, rs.getString("reason"), rs.getString("poisonedby"), true, rs.getString("removedby"), rs.getLong("placed"));

                    current++;

                }

            }

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(get, rs);

        }

        return out;

    }

    @Override
    public boolean isDiscouraged(OfflinePlayer player) throws DatabaseException {

        boolean discouraged;

        if (discouragementCache.containsKey(player.getUniqueId())) {

            Discouragement discouragement = discouragementCache.get(player.getUniqueId());

            discouraged = !discouragement.isExpired();

        } else {

            PreparedStatement get = null;
            ResultSet rs = null;

            try {

                get = c.prepareStatement("SELECT * FROM `kxstaffsuite_discouraged` WHERE `uuid` = ? AND `removed` = ?;");
                get.setString(1, player.getUniqueId().toString());
                get.setBoolean(2, false);

                rs = get.executeQuery();

                discouraged = rs.next();

            } catch (SQLException e) {

                throw new DatabaseException(e);

            } finally {

                closeSilently(get, rs);

            }

        }

        return discouraged;

    }

    @Override
    public void close() throws DatabaseException {

        try {

            c.close();

        } catch (SQLException e) {

            throw new DatabaseException(e);

        }

        thread.end();

    }

}
