package com.kaixeleron.staffsuite.bukkit.database;

import com.kaixeleron.staffsuite.bukkit.SuiteMain;
import com.kaixeleron.staffsuite.bukkit.database.data.Alt;
import com.kaixeleron.staffsuite.bukkit.database.data.Ban;
import com.kaixeleron.staffsuite.bukkit.database.data.Mute;
import com.kaixeleron.staffsuite.bukkit.database.data.Poison;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import javax.annotation.Nullable;
import java.io.File;
import java.io.IOException;
import java.util.*;

public class YAMLDatabase implements Database {

    private final File bansFolder, mutesFolder, poisonedFolder, discouragedFolder, altsFile;

    private final FileConfiguration alts;

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public YAMLDatabase(SuiteMain m) throws DatabaseException {

        m.getDataFolder().mkdir();

        bansFolder = new File(m.getDataFolder(), "bans");
        mutesFolder = new File(m.getDataFolder(), "mutes");
        poisonedFolder = new File(m.getDataFolder(), "poisoned");
        discouragedFolder = new File(m.getDataFolder(), "discouraged");
        altsFile = new File(m.getDataFolder(), "alts.yml");

        bansFolder.mkdir();
        mutesFolder.mkdir();
        poisonedFolder.mkdir();
        discouragedFolder.mkdir();

        if (!altsFile.exists()) {

            try {

                altsFile.createNewFile();

            } catch (IOException e) {

                throw new DatabaseException(e);

            }

        }

        alts = YamlConfiguration.loadConfiguration(altsFile);

    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    @Override
    public void ban(OfflinePlayer p, long expiry, String reason, String by) throws DatabaseException {

        File banFile = new File(bansFolder, p.getUniqueId().toString() + ".yml");

        if (!banFile.exists()) {

            try {

                banFile.createNewFile();

            } catch (IOException e) {

                throw new DatabaseException(e);

            }

        }

        FileConfiguration ban = YamlConfiguration.loadConfiguration(banFile);

        ban.set("banned", true);
        ban.set("expiry", expiry);
        ban.set("reason", reason);
        ban.set("by", by);
        ban.set("placed", System.currentTimeMillis());

        try {

            ban.save(banFile);

        } catch (IOException e) {

            throw new DatabaseException(e);

        }

    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    @Override
    public void mute(OfflinePlayer p, long expiry, String reason, String by) throws DatabaseException {

        File muteFile = new File(mutesFolder, p.getUniqueId().toString() + ".yml");

        if (!muteFile.exists()) {

            try {

                muteFile.createNewFile();

            } catch (IOException e) {

                throw new DatabaseException(e);

            }

        }

        FileConfiguration mute = YamlConfiguration.loadConfiguration(muteFile);

        mute.set("banned", true);
        mute.set("expiry", expiry);
        mute.set("reason", reason);
        mute.set("by", by);
        mute.set("placed", System.currentTimeMillis());

        try {

            mute.save(muteFile);

        } catch (IOException e) {

            throw new DatabaseException(e);

        }

    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    @Override
    public void poison(String ip, String reason, String by) throws DatabaseException {

        File poisonFile = new File(poisonedFolder, ip + ".yml");

        if (!poisonFile.exists()) {

            try {

                poisonFile.createNewFile();

            } catch (IOException e) {

                throw new DatabaseException(e);

            }

        }

        FileConfiguration poison = YamlConfiguration.loadConfiguration(poisonFile);

        poison.set("poisoned", true);
        poison.set("reason", reason);
        poison.set("by", by);
        poison.set("placed", System.currentTimeMillis());

        try {

            poison.save(poisonFile);

        } catch (IOException e) {

            throw new DatabaseException(e);

        }

    }

    @Override
    public void unban(OfflinePlayer p, String by) throws DatabaseException {

        File banFile = new File(bansFolder, p.getUniqueId().toString() + ".yml");

        if (banFile.exists()) {

            FileConfiguration ban = YamlConfiguration.loadConfiguration(banFile);

            if (ban.getBoolean("banned", false)) {

                ConfigurationSection pastBans = ban.getConfigurationSection("pastBans");

                if (pastBans == null) pastBans = ban.createSection("pastBans");

                int current = pastBans.getKeys(false).size();

                pastBans.set(String.format("%d.expiry", current), ban.get("expiry"));
                pastBans.set(String.format("%d.reason", current), ban.get("reason"));
                pastBans.set(String.format("%d.by", current), ban.get("by"));
                pastBans.set(String.format("%d.placed", current), ban.get("placed"));
                pastBans.set(String.format("%d.removedBy", current), by);

                ban.set("banned", null);
                ban.set("expiry", null);
                ban.set("reason", null);
                ban.set("by", null);
                ban.set("placed", null);

                try {

                    ban.save(banFile);

                } catch (IOException e) {

                    throw new DatabaseException(e);

                }

            }

        }

    }

    @Override
    public void unmute(OfflinePlayer p, String by) throws DatabaseException {

        File muteFile = new File(mutesFolder, p.getUniqueId().toString() + ".yml");

        if (muteFile.exists()) {

            FileConfiguration mute = YamlConfiguration.loadConfiguration(muteFile);

            if (mute.getBoolean("muted", false)) {

                ConfigurationSection pastMutes = mute.getConfigurationSection("pastMutes");

                if (pastMutes == null) pastMutes = mute.createSection("pastMutes");

                int current = pastMutes.getKeys(false).size();

                pastMutes.set(String.format("%d.expiry", current), mute.get("expiry"));
                pastMutes.set(String.format("%d.reason", current), mute.get("reason"));
                pastMutes.set(String.format("%d.by", current), mute.get("by"));
                pastMutes.set(String.format("%d.placed", current), mute.get("placed"));
                pastMutes.set(String.format("%d.removedBy", current), by);

                mute.set("muted", null);
                mute.set("expiry", null);
                mute.set("reason", null);
                mute.set("by", null);
                mute.set("placed", null);

                try {

                    mute.save(muteFile);

                } catch (IOException e) {

                    throw new DatabaseException(e);

                }

            }

        }

    }

    @Override
    public void unpoison(String ip, String by) throws DatabaseException {

        File poisonFile = new File(poisonedFolder, ip + ".yml");

        if (poisonFile.exists()) {

            FileConfiguration poison = YamlConfiguration.loadConfiguration(poisonFile);

            if (poison.getBoolean("poisoned", false)) {

                ConfigurationSection pastPoisons = poison.getConfigurationSection("pastPoisons");

                if (pastPoisons == null) pastPoisons = poison.createSection("pastPoisons");

                int current = pastPoisons.getKeys(false).size();

                pastPoisons.set(String.format("%d.reason", current), poison.get("reason"));
                pastPoisons.set(String.format("%d.by", current), poison.get("by"));
                pastPoisons.set(String.format("%d.placed", current), poison.get("placed"));
                pastPoisons.set(String.format("%d.removedBy", current), by);

                poison.set("poisoned", null);
                poison.set("reason", null);
                poison.set("by", null);
                poison.set("placed", null);

                try {

                    poison.save(poisonFile);

                } catch (IOException e) {

                    throw new DatabaseException(e);

                }

            }

        }

    }

    @Override
    public void updateAlt(String ip, UUID uuid, String name) throws DatabaseException {

        //TODO

    }

    @Override
    public void discourage(OfflinePlayer player, String by) throws DatabaseException {

        File discouragedFile = new File(discouragedFolder, player.getUniqueId().toString() + ".yml");

        if (!discouragedFile.exists()) {

            try {

                discouragedFile.createNewFile();

            } catch (IOException e) {

                throw new DatabaseException(e);

            }

        }

        FileConfiguration discouraged = YamlConfiguration.loadConfiguration(discouragedFile);

        discouraged.set("discouraged", true);
        discouraged.set("by", by);
        discouraged.set("placed", System.currentTimeMillis());

        try {

            discouraged.save(discouragedFile);

        } catch (IOException e) {

            throw new DatabaseException(e);

        }

    }

    @Override
    public void undiscourage(OfflinePlayer player, String by) throws DatabaseException {

        File discouragedFile = new File(discouragedFolder, player.getUniqueId().toString() + ".yml");

        if (discouragedFile.exists()) {

            FileConfiguration discouraged = YamlConfiguration.loadConfiguration(discouragedFile);

            if (discouraged.getBoolean("discouraged", false)) {

                ConfigurationSection pastDiscouragements = discouraged.getConfigurationSection("pastDiscouragements");

                if (pastDiscouragements == null) pastDiscouragements = discouraged.createSection("pastDiscouragements");

                int current = pastDiscouragements.getKeys(false).size();

                pastDiscouragements.set(String.format("%d.by", current), discouraged.get("by"));
                pastDiscouragements.set(String.format("%d.placed", current), discouraged.get("placed"));
                pastDiscouragements.set(String.format("%d.removedBy", current), by);

                discouraged.set("discouraged", null);
                discouraged.set("by", null);
                discouraged.set("placed", null);

                try {

                    discouraged.save(discouragedFile);

                } catch (IOException e) {

                    throw new DatabaseException(e);

                }

            }

        }

    }

    @Override
    public @Nullable Ban getBan(OfflinePlayer p) {

        File banFile = new File(bansFolder, p.getUniqueId().toString() + ".yml");

        if (banFile.exists()) {

            FileConfiguration ban = YamlConfiguration.loadConfiguration(banFile);

            if (ban.getBoolean("banned", false)) {

                return new Ban(p.getUniqueId(), ban.getString("reason"), ban.getLong("expiry"), ban.getString("by"), false, "", ban.getLong("placed"));

            }

        }

        return null;

    }

    @Override
    public @Nullable Mute getMute(OfflinePlayer p) {

        File muteFile = new File(mutesFolder, p.getUniqueId().toString() + ".yml");

        if (muteFile.exists()) {

            FileConfiguration mute = YamlConfiguration.loadConfiguration(muteFile);

            if (mute.getBoolean("muted", false)) {

                return new Mute(p.getUniqueId(), mute.getString("reason"), mute.getLong("expiry"), mute.getString("by"), false, "", mute.getLong("placed"));

            }

        }

        return null;

    }

    @Override
    public @Nullable Poison getPoison(String ip) {

        File poisonFile = new File(poisonedFolder, ip + ".yml");

        if (poisonFile.exists()) {

            FileConfiguration poison = YamlConfiguration.loadConfiguration(poisonFile);

            if (poison.getBoolean("poisoned", false)) {

                return new Poison(ip, poison.getString("reason"), poison.getString("by"), false, "", poison.getLong("placed"));

            }

        }

        return null;

    }

    @Override
    public Ban[] getBanHistory(OfflinePlayer p) {

        File banFile = new File(bansFolder, p.getUniqueId().toString() + ".yml");

        if (banFile.exists()) {

            ConfigurationSection bans = YamlConfiguration.loadConfiguration(banFile).getConfigurationSection("pastBans");

            if (bans != null) {

                Ban[] history = new Ban[bans.getKeys(false).size()];

                for (int i = 0; i < history.length; i++) {

                    history[i] = new Ban(p.getUniqueId(), bans.getString(String.format("%d.reason", i)), bans.getLong(String.format("%d.expiry", i)), bans.getString(String.format("%d.by", i)), true, bans.getString(String.format("%d.removedBy", i)), bans.getLong(String.format("%d.placed", i)));

                }

                return history;

            }

        }

        return new Ban[0];

    }

    @Override
    public Mute[] getMuteHistory(OfflinePlayer p) {

        File muteFile = new File(mutesFolder, p.getUniqueId().toString() + ".yml");

        if (muteFile.exists()) {

            ConfigurationSection mutes = YamlConfiguration.loadConfiguration(muteFile).getConfigurationSection("pastMutes");

            if (mutes != null) {

                Mute[] history = new Mute[mutes.getKeys(false).size()];

                for (int i = 0; i < history.length; i++) {

                    history[i] = new Mute(p.getUniqueId(), mutes.getString(String.format("%d.reason", i)), mutes.getLong(String.format("%d.expiry", i)), mutes.getString(String.format("%d.by", i)), true, mutes.getString(String.format("%d.removedBy", i)), mutes.getLong(String.format("%d.placed", i)));

                }

                return history;

            }

        }

        return new Mute[0];

    }

    //TODO
    @Override
    public Alt[] getAlts(String ip) {
        return new Alt[0];
    }

    //TODO
    @Override
    public Alt[] getAlts(UUID id) {
        return new Alt[0];
    }

    @Override
    public Poison[] getPoisonHistory(String ip) {

        File poisonFile = new File(poisonedFolder, ip + ".yml");

        if (poisonFile.exists()) {

            ConfigurationSection poisons = YamlConfiguration.loadConfiguration(poisonFile).getConfigurationSection("pastPoisons");

            if (poisons != null) {

                Poison[] history = new Poison[poisons.getKeys(false).size()];

                for (int i = 0; i < history.length; i++) {

                    history[i] = new Poison(ip, poisons.getString(String.format("%d.reason", i)), poisons.getString(String.format("%d.by", i)), true, poisons.getString(String.format("%d.removedBy", i)), poisons.getLong(String.format("%d.placed", i)));

                }

                return history;

            }

        }

        return new Poison[0];

    }

    @Override
    public boolean isDiscouraged(OfflinePlayer player) throws DatabaseException {

        boolean discouraged = false;

        File discouragedFile = new File(discouragedFolder, player.getUniqueId().toString() + ".yml");

        if (discouragedFile.exists()) {

            FileConfiguration discouragedConfig = YamlConfiguration.loadConfiguration(discouragedFile);

            if (discouragedConfig.getBoolean("discouraged", false)) {

                discouraged = true;

            }

        }

        return discouraged;

    }

    @Override
    public void close() {}

}
