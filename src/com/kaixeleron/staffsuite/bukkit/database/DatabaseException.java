package com.kaixeleron.staffsuite.bukkit.database;

public class DatabaseException extends Exception {

    private final Exception cause;

    DatabaseException(Exception cause) {

        this.cause = cause;

    }

    @Override
    public void printStackTrace() {

        cause.printStackTrace();
        super.printStackTrace();

    }

}
