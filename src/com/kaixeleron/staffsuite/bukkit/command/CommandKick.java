package com.kaixeleron.staffsuite.bukkit.command;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandKick implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (args.length < 2) {

            sender.sendMessage(ChatColor.RED + "Kick a player");
            sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + "/kick <player> <reason>");

        } else {

            Player p = Bukkit.getPlayer(args[0]);

            if (p == null) {

                sender.sendMessage(ChatColor.RED + "Player not found.");

            } else {

                StringBuilder reason = new StringBuilder();

                for (int i = 1; i < args.length; i++) {

                    reason.append(args[i]).append(" ");

                }

                p.kickPlayer(ChatColor.RED + "Kicked\n\nReason: " + ChatColor.RESET + reason);

                for (Player player : Bukkit.getOnlinePlayers()) {

                    if (player.hasPermission("kxstaffsuite.notify.kick")) {

                        player.sendMessage(sender.getName() + ChatColor.RED + " kicked " + ChatColor.RESET + p.getName() + ChatColor.RED + " for " + ChatColor.RESET + reason.toString());

                    }

                }

                Bukkit.getConsoleSender().sendMessage(sender.getName() + ChatColor.RED + " kicked " + ChatColor.RESET + p.getName() + ChatColor.RED + " for " + ChatColor.RESET + reason.toString());

                if (!sender.hasPermission("kxstaffsuite.notify.kick")) {

                    sender.sendMessage(ChatColor.RED + "Kicked " + ChatColor.RESET + p.getName() + ChatColor.RED + " for " + ChatColor.RESET + reason.toString());

                }

            }

        }

        return true;
    }

}
