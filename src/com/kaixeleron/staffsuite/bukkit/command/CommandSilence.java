package com.kaixeleron.staffsuite.bukkit.command;

import com.kaixeleron.staffsuite.bukkit.listener.ChatListener;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class CommandSilence implements CommandExecutor {

    private final ChatListener listener;

    public CommandSilence(ChatListener listener) {

        this.listener = listener;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (listener.isSilenced()) {

            Bukkit.broadcastMessage(ChatColor.RED + "The server is no longer silenced.");
            listener.setSilenced(false);

        } else {

            Bukkit.broadcastMessage(ChatColor.RED + "The server is now silenced.");
            listener.setSilenced(true);

        }

        return true;

    }

}
