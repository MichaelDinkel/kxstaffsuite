package com.kaixeleron.staffsuite.bukkit.command;

import com.kaixeleron.staffsuite.bukkit.database.Database;
import com.kaixeleron.staffsuite.bukkit.database.DatabaseException;
import com.kaixeleron.staffsuite.bukkit.database.data.Mute;
import com.kaixeleron.staffsuite.time.TimeManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class CommandMuteHistory implements CommandExecutor {

    private final Database db;

    private final TimeManager tm;

    public CommandMuteHistory(Database db, TimeManager tm) {

        this.db = db;
        this.tm = tm;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (args.length == 0) {

            sender.sendMessage(ChatColor.RED + "Check a player's mute history");
            sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + "/mutehistory <player>");

        } else {

            @SuppressWarnings("deprecation") OfflinePlayer p = Bukkit.getOfflinePlayer(args[0]);

            try {

                Mute[] mutes = db.getMuteHistory(p);

                if (mutes.length == 0) {

                    sender.sendMessage(args[0] + ChatColor.RED + " has no mute history.");

                } else {

                    sender.sendMessage(ChatColor.RED + "Mute history for " + ChatColor.RESET + args[0]);

                    long currentTime = System.currentTimeMillis();

                    for (int i = 0; i < mutes.length; i++) {

                        Mute m = mutes[i];

                        sender.sendMessage(ChatColor.YELLOW + String.format("%d", i + 1) + ". "
                                + ChatColor.RED + "Placed " + ChatColor.RESET + tm.timeToString(currentTime
                                - m.getPlaced()) + ChatColor.RED + " ago by " + ChatColor.RESET + m.getBy()
                                + ChatColor.RED + " with duration " + ChatColor.RESET + (m.getExpiry() == -1 ?  "permanent" : tm.timeToString(m.getExpiry()
                                - m.getPlaced())) + ChatColor.RED + " for reason " + ChatColor.RESET + m.getReason()
                                + (m.isExpired() ? ChatColor.RED + " expired by " + ChatColor.RESET + m.getExpiredBy()
                                : ChatColor.GREEN + "Active"));

                    }

                }

            } catch (DatabaseException e) {

                sender.sendMessage(ChatColor.RED + "A database error has occurred.");
                e.printStackTrace();

            }

        }

        return true;

    }

}
