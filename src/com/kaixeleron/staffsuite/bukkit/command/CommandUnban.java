package com.kaixeleron.staffsuite.bukkit.command;

import com.kaixeleron.staffsuite.bukkit.SuiteMain;
import com.kaixeleron.staffsuite.bukkit.database.Database;
import com.kaixeleron.staffsuite.bukkit.database.DatabaseException;
import org.bukkit.BanList;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Date;

public class CommandUnban implements CommandExecutor {

    private final SuiteMain m;

    private final Database db;

    public CommandUnban(SuiteMain m, Database db) {

        this.m = m;

        this.db = db;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (args.length == 0) {

            sender.sendMessage(ChatColor.RED + "Unban a player");
            sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + "/unban <player>");

        } else {

            @SuppressWarnings("deprecation") OfflinePlayer p = Bukkit.getOfflinePlayer(args[0]);

            try {

                if (db.getBan(p) == null) {

                    sender.sendMessage(args[0] + ChatColor.RED + " is not banned.");

                } else {

                    db.unban(p, sender.getName());

                    if (m.getConfig().getBoolean("useVanillaBans")) {

                        Bukkit.getBanList(BanList.Type.NAME).pardon(args[0]);

                    }

                    for (Player player : Bukkit.getOnlinePlayers()) {

                        if (player.hasPermission("kxstaffsuite.notify.unban")) {

                            player.sendMessage(sender.getName() + ChatColor.RED + " unbanned " + ChatColor.RESET + p.getName());

                        }

                    }

                    Bukkit.getConsoleSender().sendMessage(sender.getName() + ChatColor.RED + " unbanned " + ChatColor.RESET + p.getName());

                    if (!sender.hasPermission("kxstaffsuite.notify.unban")) {

                        sender.sendMessage(ChatColor.RED + "Unbanned " + ChatColor.RESET + p.getName());

                    }

                }

            } catch (DatabaseException e) {

                sender.sendMessage(ChatColor.RED + "A database error has occurred.");
                e.printStackTrace();

            }

        }

        return true;

    }

}
