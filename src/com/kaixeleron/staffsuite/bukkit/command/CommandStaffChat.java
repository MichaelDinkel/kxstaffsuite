package com.kaixeleron.staffsuite.bukkit.command;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandStaffChat implements CommandExecutor {

    private final ChatColor color;

    private final String character;

    public CommandStaffChat(ChatColor color, String character) {

        this.color = color;

        this.character = character;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (args.length == 0) {

            sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + String.format("/%s <message>", character));

        } else {

            StringBuilder message = new StringBuilder();

            for (String s : args) {

                message.append(s).append(" ");

            }

            String chatMessage = "<" + color + sender.getName() + ChatColor.RESET + "> " + color + message.toString();

            for (Player p : Bukkit.getOnlinePlayers()) {

                if (p.hasPermission(String.format("kxstaffsuite.command.staffchat.%s", character))) {

                    p.sendMessage(chatMessage);

                }

            }

            Bukkit.getConsoleSender().sendMessage(chatMessage);

        }

        return true;
    }

}
