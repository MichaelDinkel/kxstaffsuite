package com.kaixeleron.staffsuite.bukkit.command;

import com.kaixeleron.staffsuite.bukkit.database.data.Ban;
import com.kaixeleron.staffsuite.bukkit.database.Database;
import com.kaixeleron.staffsuite.bukkit.database.DatabaseException;
import com.kaixeleron.staffsuite.time.TimeManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class CommandBanHistory implements CommandExecutor {

    private final Database db;

    private final TimeManager tm;

    public CommandBanHistory(Database db, TimeManager tm) {

        this.db = db;
        this.tm = tm;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (args.length == 0) {

            sender.sendMessage(ChatColor.RED + "Check a player's ban history");
            sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + "/banhistory <player>");

        } else {

            @SuppressWarnings("deprecation") OfflinePlayer p = Bukkit.getOfflinePlayer(args[0]);

            try {

                Ban[] bans = db.getBanHistory(p);

                if (bans.length == 0) {

                    sender.sendMessage(args[0] + ChatColor.RED + " has no ban history.");

                } else {

                    sender.sendMessage(ChatColor.RED + "Ban history for " + ChatColor.RESET + args[0]);

                    long currentTime = System.currentTimeMillis();

                    for (int i = 0; i < bans.length; i++) {

                        Ban b = bans[i];

                        sender.sendMessage(ChatColor.YELLOW + String.format("%d. ", i + 1)
                                + ChatColor.RED + "Placed " + ChatColor.RESET + tm.timeToString(currentTime
                                - b.getPlaced()) + ChatColor.RED + " ago by " + ChatColor.RESET + b.getBy()
                                + ChatColor.RED + " with duration " + ChatColor.RESET + (b.getExpiry() == -1 ? "permanent" : tm.timeToString(b.getExpiry()
                                - b.getPlaced())) + ChatColor.RED + " for reason " + ChatColor.RESET + b.getReason()
                                + (b.isExpired() ? ChatColor.RED + " expired by " + ChatColor.RESET + b.getExpiredBy()
                                : ChatColor.GREEN + "Active"));

                    }

                }

            } catch (DatabaseException e) {

                sender.sendMessage(ChatColor.RED + "A database error has occurred.");
                e.printStackTrace();

            }

        }

        return true;

    }

}
