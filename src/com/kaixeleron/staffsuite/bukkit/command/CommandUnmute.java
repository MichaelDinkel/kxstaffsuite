package com.kaixeleron.staffsuite.bukkit.command;

import com.kaixeleron.staffsuite.bukkit.database.Database;
import com.kaixeleron.staffsuite.bukkit.database.DatabaseException;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandUnmute implements CommandExecutor {

    private final Database db;

    public CommandUnmute(Database db) {

        this.db = db;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (args.length == 0) {

            sender.sendMessage(ChatColor.RED + "Unmute a player");
            sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + "/unmute <player>");

        } else {

            @SuppressWarnings("deprecation") OfflinePlayer p = Bukkit.getOfflinePlayer(args[0]);

            try {

                if (db.getMute(p) == null) {

                    sender.sendMessage(args[0] + ChatColor.RED + " is not muted.");

                } else {

                    db.unmute(p, sender.getName());

                    for (Player player : Bukkit.getOnlinePlayers()) {

                        if (player.hasPermission("kxstaffsuite.notify.unmute")) {

                            player.sendMessage(sender.getName() + ChatColor.RED + " unmuted " + ChatColor.RESET + p.getName());

                        }

                    }

                    Bukkit.getConsoleSender().sendMessage(sender.getName() + ChatColor.RED + " unmuted " + ChatColor.RESET + p.getName());

                    if (!sender.hasPermission("kxstaffsuite.notify.unmute")) {

                        sender.sendMessage(ChatColor.RED + "Unmuted " + ChatColor.RESET + p.getName());

                    }

                }

            } catch (DatabaseException e) {

                sender.sendMessage(ChatColor.RED + "A database error has occurred.");
                e.printStackTrace();

            }

        }

        return true;

    }

}
