package com.kaixeleron.staffsuite.bukkit.command;

import com.google.common.net.InetAddresses;
import com.kaixeleron.staffsuite.bukkit.database.Database;
import com.kaixeleron.staffsuite.bukkit.database.DatabaseException;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandUnpoison implements CommandExecutor {

    private final Database db;

    public CommandUnpoison(Database db) {

        this.db = db;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (args.length == 0) {

            sender.sendMessage(ChatColor.RED + "Unpoison an IP address");
            sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + "/unpoison <IP>");

        } else {

            if (InetAddresses.isInetAddress(args[0])) {

                try {

                    if (db.getPoison(args[0].toLowerCase()) == null) {

                        sender.sendMessage(args[0] + ChatColor.RED + " is not poisoned.");

                    } else {

                        db.unpoison(args[0].toLowerCase(), sender.getName());

                        for (Player player : Bukkit.getOnlinePlayers()) {

                            if (player.hasPermission("kxstaffsuite.notify.unpoison")) {

                                player.sendMessage(sender.getName() + ChatColor.RED + " unpoisoned " + ChatColor.RESET + args[0].toLowerCase());

                            }

                        }

                        Bukkit.getConsoleSender().sendMessage(sender.getName() + ChatColor.RED + " unpoisoned " + ChatColor.RESET + args[0].toLowerCase());

                        if (!sender.hasPermission("kxstaffsuite.notify.unpoison")) {

                            sender.sendMessage(ChatColor.RED + "Unpoisoned " + ChatColor.RESET + args[0].toLowerCase());

                        }

                    }

                } catch (DatabaseException e) {

                    sender.sendMessage(ChatColor.RED + "A database error has occurred.");
                    e.printStackTrace();

                }

            } else {

                sender.sendMessage(ChatColor.RED + "You must specify a valid IP address to unpoison.");

            }

        }

        return true;

    }

}
