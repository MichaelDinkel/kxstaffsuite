package com.kaixeleron.staffsuite.bukkit.command;

import com.google.common.net.InetAddresses;
import com.kaixeleron.staffsuite.bukkit.database.Database;
import com.kaixeleron.staffsuite.bukkit.database.DatabaseException;
import com.kaixeleron.staffsuite.bukkit.database.data.Alt;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.util.Calendar;

public class CommandAlts implements CommandExecutor {

    private final Database db;

    public CommandAlts(Database db) {

        this.db = db;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (args.length == 0) {

            sender.sendMessage(ChatColor.RED + "Check for alts by name" + (sender.hasPermission("kxstaffsuite.command.alts.ip") ? " or IP address" : ""));
            sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + "/alts <player" + (sender.hasPermission("kxstaffsuite.command.alts.ip") ? "|IP" : "") + ">");

        } else {

            if (InetAddresses.isInetAddress(args[0]) && sender.hasPermission("kxstaffsuite.command.alts.ip")) {

                try {

                    Alt[] alts = db.getAlts(args[0]);

                    if (alts.length > 0) {

                        sender.sendMessage(ChatColor.RED + "Accounts on IP " + ChatColor.RESET + args[0]);

                        showAlts(sender, alts);

                    } else {

                        sender.sendMessage(ChatColor.RED + "No accounts found on IP " + ChatColor.RESET + args[0]);

                    }

                } catch (DatabaseException e) {

                    sender.sendMessage(ChatColor.RED + "A database error has occurred.");
                    e.printStackTrace();

                }

            } else {

                try {

                    @SuppressWarnings("deprecation") Alt[] alts = db.getAlts(Bukkit.getOfflinePlayer(args[0]).getUniqueId());

                    if (alts.length > 0) {

                        if (sender.hasPermission("kxstaffsuite.command.alts.ip")) sender.sendMessage(ChatColor.RED + "IP address: " + ChatColor.RESET + alts[0].getIp());

                        sender.sendMessage(ChatColor.RED + "Accounts for player " + ChatColor.RESET + args[0]);

                        showAlts(sender, alts);

                    } else {

                        sender.sendMessage(ChatColor.RED + "No accounts found for player " + ChatColor.RESET + args[0]);

                    }

                } catch (DatabaseException e) {

                    sender.sendMessage(ChatColor.RED + "A database error has occurred.");
                    e.printStackTrace();

                }

            }

        }

        return true;

    }

    private void showAlts(CommandSender sender, Alt[] alts) {

        for (Alt a : alts) {

            TextComponent name = new TextComponent(a.getName());
            name.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(a.getId().toString()).create()));
            TextComponent logintext = new TextComponent(" last login ");
            logintext.setColor(net.md_5.bungee.api.ChatColor.RED);

            Calendar c = Calendar.getInstance();
            c.setTimeInMillis(a.getLastLogin());
            TextComponent login = new TextComponent(String.format("%d/%d/%d", c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH) + 1, c.get(Calendar.YEAR)));
            login.setColor(net.md_5.bungee.api.ChatColor.RESET);

            name.addExtra(logintext);
            name.addExtra(login);

            sender.spigot().sendMessage(name);

        }

    }

}
