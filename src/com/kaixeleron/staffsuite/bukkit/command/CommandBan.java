package com.kaixeleron.staffsuite.bukkit.command;

import com.kaixeleron.staffsuite.bukkit.SuiteMain;
import com.kaixeleron.staffsuite.bukkit.database.Database;
import com.kaixeleron.staffsuite.bukkit.database.DatabaseException;
import org.bukkit.BanList;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandBan implements CommandExecutor {

    private final SuiteMain m;

    private final Database db;

    public CommandBan(SuiteMain m, Database db) {

        this.m = m;

        this.db = db;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (args.length < 2) {

            sender.sendMessage(ChatColor.RED + "Permanently ban a player");
            sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + "/ban <player> <reason>");

        } else {

            @SuppressWarnings("deprecation") OfflinePlayer p = Bukkit.getOfflinePlayer(args[0]);

            StringBuilder reason = new StringBuilder();

            for (int i = 1; i < args.length; i++) {

                reason.append(args[i]).append(" ");

            }

            try {

                if (db.getBan(p) != null) {

                    sender.sendMessage(args[0] + ChatColor.RED + " is already banned.");

                } else {

                    db.ban(p, -1L, reason.substring(0, reason.length() - 1), sender.getName());

                    if (m.getConfig().getBoolean("useVanillaBans")) {

                        Bukkit.getBanList(BanList.Type.NAME).addBan(args[0], reason.toString(), null, sender.getName());

                    }

                    for (Player player : Bukkit.getOnlinePlayers()) {

                        if (player.hasPermission("kxstaffsuite.notify.ban")) {

                            player.sendMessage(sender.getName() + ChatColor.RED + " banned " + ChatColor.RESET + p.getName() + ChatColor.RED + " for " + ChatColor.RESET + reason.toString());

                        }

                    }

                    Bukkit.getConsoleSender().sendMessage(sender.getName() + ChatColor.RED + " banned " + ChatColor.RESET + p.getName() + ChatColor.RED + " for " + ChatColor.RESET + reason.toString());

                    if (!sender.hasPermission("kxstaffsuite.notify.ban")) {

                        sender.sendMessage(ChatColor.RED + "Banned " + ChatColor.RESET + p.getName() + ChatColor.RED + " for " + ChatColor.RESET + reason.toString());

                    }

                    if (p.isOnline())
                        p.getPlayer().kickPlayer(ChatColor.RED + "Banned\n\nReason: " + ChatColor.RESET + reason.toString());

                }

            } catch (DatabaseException e) {

                sender.sendMessage(ChatColor.RED + "A database error has occurred.");
                e.printStackTrace();

            }

        }

        return true;

    }

}
