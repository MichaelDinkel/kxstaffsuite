package com.kaixeleron.staffsuite.bukkit.command;

import com.kaixeleron.staffsuite.bukkit.SuiteMain;
import com.kaixeleron.staffsuite.bukkit.database.Database;
import com.kaixeleron.staffsuite.bukkit.database.DatabaseException;
import com.kaixeleron.staffsuite.time.TimeManager;
import org.bukkit.BanList;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Date;

public class CommandTempban implements CommandExecutor {

    private final SuiteMain m;

    private final Database db;

    private final TimeManager timeManager;

    public CommandTempban(SuiteMain m, Database db, TimeManager timeManager) {

        this.m = m;

        this.db = db;

        this.timeManager = timeManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (args.length < 3) {

            sender.sendMessage(ChatColor.RED + "Temporarily ban a player");
            sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + "/tempban <player> <time> <reason>");

        } else {

            @SuppressWarnings("deprecation") OfflinePlayer p = Bukkit.getOfflinePlayer(args[0]);

            try {

                String timeStr = args[1].toLowerCase();

                long time = timeManager.parseTime(timeStr) + System.currentTimeMillis();

                StringBuilder reason = new StringBuilder();

                for (int i = 2; i < args.length; i++) {

                    reason.append(args[i]).append(" ");

                }

                try {

                    if (db.getBan(p) != null) {

                        sender.sendMessage(args[0] + ChatColor.RED + " is already banned.");

                    } else {

                        db.ban(p, time, reason.substring(0, reason.length() - 1), sender.getName());

                        if (m.getConfig().getBoolean("useVanillaBans")) {

                            Bukkit.getBanList(BanList.Type.NAME).addBan(args[0], reason.toString(), new Date(time), sender.getName());

                        }

                        for (Player player : Bukkit.getOnlinePlayers()) {

                            if (player.hasPermission("kxstaffsuite.notify.tempban")) {

                                player.sendMessage(sender.getName() + ChatColor.RED + " temporarily banned " + ChatColor.RESET + p.getName() + ChatColor.RED + " for " + ChatColor.RESET
                                        + timeStr + ChatColor.RED + " for " + ChatColor.RESET + reason.toString());

                            }

                        }

                        Bukkit.getConsoleSender().sendMessage(sender.getName() + ChatColor.RED + " temporarily banned " + ChatColor.RESET + p.getName() + ChatColor.RED + " for " + ChatColor.RESET
                                + timeStr + ChatColor.RED + " for " + ChatColor.RESET + reason.toString());

                        if (!sender.hasPermission("kxstaffsuite.notify.tempban")) {

                            sender.sendMessage(ChatColor.RED + "Temporarily banned " + ChatColor.RESET + p.getName() + ChatColor.RED + " for " + ChatColor.RESET
                                    + timeStr + ChatColor.RED + " for " + ChatColor.RESET + reason.toString());

                        }

                        if (p.isOnline()) {

                            p.getPlayer().kickPlayer(ChatColor.RED + "Temporarily banned for " + ChatColor.RESET + timeManager.timeToString(time - System.currentTimeMillis()) + ChatColor.RED + "\n\nReason: " + ChatColor.RESET + reason.toString());

                        }

                    }

                } catch (DatabaseException e) {

                    sender.sendMessage(ChatColor.RED + "A database error has occurred.");
                    e.printStackTrace();

                }

            } catch (IllegalArgumentException e) {

                sender.sendMessage(ChatColor.RED + "Invalid time. Must be formatted similar to " + ChatColor.WHITE + "1h1m1s" + ChatColor.RED + ".");

            }

        }

        return true;

    }

}
