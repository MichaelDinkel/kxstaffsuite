package com.kaixeleron.staffsuite.bukkit.command;

import com.kaixeleron.staffsuite.bukkit.database.Database;
import com.kaixeleron.staffsuite.bukkit.database.DatabaseException;
import com.kaixeleron.staffsuite.bukkit.external.DiscouragementHandler;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandUndiscourage implements CommandExecutor {

    private final Database database;
    private final DiscouragementHandler handler;

    public CommandUndiscourage(Database database, DiscouragementHandler handler) {

        this.database = database;
        this.handler = handler;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (args.length == 0) {

            sender.sendMessage(ChatColor.RED + cmd.getDescription());
            sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + cmd.getUsage());

        } else {

            @SuppressWarnings("deprecation") OfflinePlayer p = Bukkit.getOfflinePlayer(args[0]);

            try {

                if (!database.isDiscouraged(p)) {

                    sender.sendMessage(args[0] + ChatColor.RED + " is not discouraged.");

                } else {

                    database.undiscourage(p, sender.getName());

                    if (handler != null && p.isOnline()) {

                        handler.removeDiscouragement(p.getPlayer());

                    }

                    for (Player player : Bukkit.getOnlinePlayers()) {

                        if (player.hasPermission("kxstaffsuite.notify.undiscourage")) {

                            player.sendMessage(sender.getName() + ChatColor.RED + " has undiscouraged " + ChatColor.RESET + p.getName());

                        }

                    }

                    Bukkit.getConsoleSender().sendMessage(sender.getName() + ChatColor.RED + " has undiscouraged " + ChatColor.RESET + p.getName());

                    if (!sender.hasPermission("kxstaffsuite.notify.undiscourage")) {

                        sender.sendMessage(ChatColor.RED + "Undiscourage " + ChatColor.RESET + p.getName());

                    }

                }

            } catch (DatabaseException e) {

                sender.sendMessage(ChatColor.RED + "A database error has occurred.");
                e.printStackTrace();

            }

        }

        return true;

    }
}
