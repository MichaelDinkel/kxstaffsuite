package com.kaixeleron.staffsuite.bukkit.command;

import com.kaixeleron.staffsuite.bukkit.database.Database;
import com.kaixeleron.staffsuite.bukkit.database.DatabaseException;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandMute implements CommandExecutor {

    private final Database db;

    public CommandMute(Database db) {

        this.db = db;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (args.length < 2) {

            sender.sendMessage(ChatColor.RED + "Permanently mute a player");
            sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + "/mute <player> <reason>");

        } else {

            @SuppressWarnings("deprecation") OfflinePlayer p = Bukkit.getOfflinePlayer(args[0]);

            StringBuilder reason = new StringBuilder();

            for (int i = 1; i < args.length; i++) {

                reason.append(args[i]).append(" ");

            }

            try {

                if (db.getMute(p) != null) {

                    sender.sendMessage(args[0] + ChatColor.RED + " is already muted.");

                } else {

                    db.mute(p, -1L, reason.substring(0, reason.length() - 1), sender.getName());

                    for (Player player : Bukkit.getOnlinePlayers()) {

                        if (player.hasPermission("kxstaffsuite.notify.mute")) {

                            player.sendMessage(sender.getName() + ChatColor.RED + " muted " + ChatColor.RESET + p.getName() + ChatColor.RED + " for " + ChatColor.RESET + reason.toString());

                        }

                    }

                    Bukkit.getConsoleSender().sendMessage(sender.getName() + ChatColor.RED + " muted " + ChatColor.RESET + p.getName() + ChatColor.RED + " for " + ChatColor.RESET + reason.toString());

                    if (!sender.hasPermission("kxstaffsuite.notify.mute")) {

                        sender.sendMessage(ChatColor.RED + "Muted " + ChatColor.RESET + p.getName() + ChatColor.RED + " for " + ChatColor.RESET + reason.toString());

                    }

                    if (p.isOnline())
                        p.getPlayer().sendMessage(new String[]{ChatColor.RED + "You have been permanently muted.", ChatColor.RED + "Reason: " + ChatColor.RESET + reason.toString()});

                }

            } catch (DatabaseException e) {

                sender.sendMessage(ChatColor.RED + "A database error has occurred.");
                e.printStackTrace();

            }

        }

        return true;

    }

}
