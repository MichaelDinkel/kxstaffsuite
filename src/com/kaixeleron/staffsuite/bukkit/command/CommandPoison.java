package com.kaixeleron.staffsuite.bukkit.command;

import com.google.common.net.InetAddresses;
import com.kaixeleron.staffsuite.bukkit.database.Database;
import com.kaixeleron.staffsuite.bukkit.database.DatabaseException;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandPoison implements CommandExecutor {

    private final Database db;

    public CommandPoison(Database db) {

        this.db = db;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (args.length < 2) {

            sender.sendMessage(ChatColor.RED + "Poison an IP address");
            sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + "/poison <IP> <reason>");

        } else {

            if (InetAddresses.isInetAddress(args[0])) {

                StringBuilder reason = new StringBuilder();

                for (int i = 1; i < args.length; i++) {

                    reason.append(args[i]).append(" ");

                }

                try {

                    if (db.getPoison(args[0].toLowerCase()) != null) {

                        sender.sendMessage(args[0] + ChatColor.RED + " is already poisoned.");

                    } else {

                        db.poison(args[0].toLowerCase(), reason.substring(0, reason.length() - 1), sender.getName());

                        for (Player p : Bukkit.getOnlinePlayers()) {

                            String ip = p.getAddress().getAddress().toString().substring(1);

                            if (ip.contains(":")) ip = ip.split("[%]")[0]; //IPv6

                            if (ip.equals(args[0].toLowerCase())) {

                                db.ban(p, -1L, reason.toString(), sender.getName());

                                p.kickPlayer(ChatColor.RED + "Banned\n\nReason: " + ChatColor.RESET + reason.toString());


                            } else if (p.hasPermission("kxstaffsuite.notify.poison")) {

                                p.sendMessage(sender.getName() + ChatColor.RED + " poisoned " + ChatColor.RESET + args[0].toLowerCase() + ChatColor.RED + " for " + ChatColor.RESET + reason.toString());

                            }

                        }

                        Bukkit.getConsoleSender().sendMessage(sender.getName() + ChatColor.RED + " poisoned " + ChatColor.RESET + args[0].toLowerCase() + ChatColor.RED + " for " + ChatColor.RESET + reason.toString());

                        if (!sender.hasPermission("kxstaffsuite.notify.poison")) {

                            sender.sendMessage(ChatColor.RED + "Poisoned " + ChatColor.RESET + args[0].toLowerCase() + ChatColor.RED + " for " + ChatColor.RESET + reason.toString());

                        }

                    }

                } catch (DatabaseException e) {

                    sender.sendMessage(ChatColor.RED + "A database error has occurred.");
                    e.printStackTrace();

                }

            } else {

                sender.sendMessage(ChatColor.RED + "You must specify a valid IP address to poison.");

            }

        }

        return true;

    }

}
