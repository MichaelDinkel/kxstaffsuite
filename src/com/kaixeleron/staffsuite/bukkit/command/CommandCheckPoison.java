package com.kaixeleron.staffsuite.bukkit.command;

import com.kaixeleron.staffsuite.bukkit.database.data.Poison;
import com.kaixeleron.staffsuite.bukkit.database.Database;
import com.kaixeleron.staffsuite.bukkit.database.DatabaseException;
import com.kaixeleron.staffsuite.time.TimeManager;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class CommandCheckPoison implements CommandExecutor {

    private final Database db;

    private final TimeManager tm;

    public CommandCheckPoison(Database db, TimeManager tm) {

        this.db = db;
        this.tm = tm;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (args.length == 0) {

            sender.sendMessage(ChatColor.RED + "Check the poison status and history of an IP address");
            sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + "/checkpoison <IP>");

        } else {

            try {

                Poison poison = db.getPoison(args[0].toLowerCase());
                Poison[] history = db.getPoisonHistory(args[0].toLowerCase());

                if (poison == null) {

                    sender.sendMessage(args[0].toLowerCase() + ChatColor.RED + " is not poisoned.");

                } else {

                    sender.sendMessage(args[0].toLowerCase() + ChatColor.RED + " has been poisoned by " + ChatColor.RESET + poison.getBy() + ChatColor.RED + " for " + ChatColor.RESET + poison.getReason());

                }

                if (history.length > 0) {

                    sender.sendMessage(ChatColor.RED + "Poison history:");

                    for (int i = 0; i < history.length; i++) {

                        Poison p = history[i];

                        sender.sendMessage(ChatColor.YELLOW + String.format("%d. ", i + 1)
                                + ChatColor.RED + "Placed " + ChatColor.RESET + tm.timeToString(System.currentTimeMillis()
                                - p.getPlaced()) + ChatColor.RED + " ago by " + ChatColor.RESET + p.getBy()
                                + ChatColor.RED + " for reason " + ChatColor.RESET + p.getReason()
                                + ChatColor.RED + " expired by " + ChatColor.RESET + p.getExpiredBy());

                    }

                }

            } catch (DatabaseException e) {

                sender.sendMessage(ChatColor.RED + "A database error has occurred.");
                e.printStackTrace();

            }

        }

        return true;

    }

}
