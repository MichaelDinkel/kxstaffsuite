package com.kaixeleron.staffsuite.bukkit.command;

import com.kaixeleron.staffsuite.bukkit.database.data.Ban;
import com.kaixeleron.staffsuite.bukkit.database.Database;
import com.kaixeleron.staffsuite.bukkit.database.DatabaseException;
import com.kaixeleron.staffsuite.bukkit.database.data.Mute;
import com.kaixeleron.staffsuite.time.TimeManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class CommandCheck implements CommandExecutor {

    private final Database db;

    private final TimeManager tm;

    public CommandCheck(Database db, TimeManager tm) {

        this.db = db;
        this.tm = tm;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (args.length == 0) {

            sender.sendMessage(ChatColor.RED + "Check a player's ban or mute");
            sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + "/check <player>");

        } else {

            @SuppressWarnings("deprecation") OfflinePlayer p = Bukkit.getOfflinePlayer(args[0]);

            try {

                Mute mute = db.getMute(p);
                Ban ban = db.getBan(p);

                sender.sendMessage(ChatColor.RED + "Moderation status for " + ChatColor.RESET + args[0]);

                if (mute == null) {

                    sender.sendMessage(ChatColor.RED + "Muted: " + ChatColor.RESET + "no");

                } else {

                    long expiry = mute.getExpiry();

                    sender.sendMessage(ChatColor.RED + "Muted: " + ChatColor.RESET + "yes, " + (expiry == -1L ? "permanently"
                            : "with " + tm.timeToString(expiry - System.currentTimeMillis()) + " remaining") + ChatColor.RED
                            + " by " + ChatColor.RESET + mute.getBy() + ChatColor.RED + " for " + ChatColor.RESET + mute.getReason());

                }

                if (ban == null) {

                    sender.sendMessage(ChatColor.RED + "Banned: " + ChatColor.RESET + "no");

                } else {

                    long expiry = ban.getExpiry();

                    sender.sendMessage(ChatColor.RED + "Banned: " + ChatColor.RESET + "yes, " + (expiry == -1L ? "permanently"
                            : "with " + tm.timeToString(expiry - System.currentTimeMillis()) + " remaining") + ChatColor.RED
                            + " by " + ChatColor.RESET + ban.getBy() + ChatColor.RED + " for " + ChatColor.RESET + ban.getReason());

                }

            } catch (DatabaseException e) {

                sender.sendMessage(ChatColor.RED + "A database error has occurred.");
                e.printStackTrace();

            }

        }

        return true;

    }

}
