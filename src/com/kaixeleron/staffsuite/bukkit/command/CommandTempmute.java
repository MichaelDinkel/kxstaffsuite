package com.kaixeleron.staffsuite.bukkit.command;

import com.kaixeleron.staffsuite.bukkit.database.DatabaseException;
import com.kaixeleron.staffsuite.bukkit.database.Database;
import com.kaixeleron.staffsuite.time.TimeManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandTempmute implements CommandExecutor {

    private final Database db;

    private final TimeManager timeManager;

    public CommandTempmute(Database db, TimeManager timeManager) {

        this.db = db;

        this.timeManager = timeManager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (args.length < 3) {

            sender.sendMessage(ChatColor.RED + "Temporarily mute a player");
            sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + "/tempmute <player> <time> <reason>");

        } else {

            @SuppressWarnings("deprecation") OfflinePlayer p = Bukkit.getOfflinePlayer(args[0]);

            try {

                String timeStr = args[1].toLowerCase();

                long time = timeManager.parseTime(timeStr) + System.currentTimeMillis();

                StringBuilder reason = new StringBuilder();

                for (int i = 2; i < args.length; i++) {

                    reason.append(args[i]).append(" ");

                }

                try {

                    if (db.getMute(p) != null) {

                        sender.sendMessage(args[0] + ChatColor.RED + " is already muted.");

                    } else {

                        db.mute(p, time, reason.substring(0, reason.length() - 1), sender.getName());

                        for (Player player : Bukkit.getOnlinePlayers()) {

                            if (player.hasPermission("kxstaffsuite.notify.tempmute")) {

                                player.sendMessage(sender.getName() + ChatColor.RED + " temporarily muted " + ChatColor.RESET + p.getName() + ChatColor.RED + " for " + ChatColor.RESET
                                        + timeStr + ChatColor.RED + " for " + ChatColor.RESET + reason.toString());

                            }

                        }

                        Bukkit.getConsoleSender().sendMessage(sender.getName() + ChatColor.RED + " temporarily muted " + ChatColor.RESET + p.getName() + ChatColor.RED + " for " + ChatColor.RESET
                                + timeStr + ChatColor.RED + " for " + ChatColor.RESET + reason.toString());

                        if (!sender.hasPermission("kxstaffsuite.notify.tempmute")) {

                            sender.sendMessage(ChatColor.RED + "Temporarily muted " + ChatColor.RESET + p.getName() + ChatColor.RED + " for " + ChatColor.RESET
                                    + timeStr + ChatColor.RED + " for " + ChatColor.RESET + reason.toString());

                        }

                        if (p.isOnline())
                            p.getPlayer().sendMessage(new String[]{ChatColor.RED + "You have been temporarily muted.", ChatColor.RED + "Duration: "
                                    + ChatColor.RESET + timeManager.timeToString(time - System.currentTimeMillis()), ChatColor.RED + "Reason: " + ChatColor.RESET + reason.toString()});

                    }

                } catch (DatabaseException e) {

                    sender.sendMessage(ChatColor.RED + "A database error has occurred.");
                    e.printStackTrace();

                }

            } catch (IllegalArgumentException e) {

                sender.sendMessage(ChatColor.RED + "Invalid time. Must be formatted similar to " + ChatColor.WHITE + "1h1m1s" + ChatColor.RED + ".");

            }

        }

        return true;

    }

}
