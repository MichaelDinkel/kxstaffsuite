package com.kaixeleron.staffsuite.time;

public class TimeManager {

    private static final long MILLISECONDS_PER_SECOND = 1000L;
    private static final long MILLISECONDS_PER_MINUTE = 60000L;
    private static final long MILLISECONDS_PER_HOUR = 3600000L;
    private static final long MILLISECONDS_PER_DAY = 86400000L;

    public long parseTime(String time) {

        char[] chars = time.toCharArray();

        long seconds = 0, minutes = 0, hours = 0, days = 0;

        long currentTime = 0;

        for (char c : chars) {

            switch (c) {

                case 's':

                    seconds = currentTime;
                    currentTime = 0;

                    break;

                case 'm':

                    minutes = currentTime;
                    currentTime = 0;

                    break;

                case 'h':

                    hours = currentTime;
                    currentTime = 0;

                    break;

                case 'd':

                    days = currentTime;
                    currentTime = 0;

                    break;

                default:

                    long digit = getDigit(c);

                    if (digit != -1) {

                        currentTime *= 10;
                        currentTime += digit;

                    }

                    break;

            }

        }

        return seconds * MILLISECONDS_PER_SECOND + minutes * MILLISECONDS_PER_MINUTE + hours * MILLISECONDS_PER_HOUR + days * MILLISECONDS_PER_DAY;

    }

    private long getDigit(char character) {

        long digit = -1L;

        switch (character) {

            case '0':

                digit = 0L;
                break;

            case '1':

                digit = 1L;
                break;

            case '2':

                digit = 2L;
                break;

            case '3':

                digit = 3L;
                break;

            case '4':

                digit = 4L;
                break;

            case '5':

                digit = 5L;
                break;

            case '6':

                digit = 6L;
                break;

            case '7':

                digit = 7L;
                break;

            case '8':

                digit = 8L;
                break;

            case '9':

                digit = 9L;
                break;

        }

        return digit;

    }

    public String timeToString(long time) {

        long days = time / MILLISECONDS_PER_DAY;
        long hours = (time % MILLISECONDS_PER_DAY) / MILLISECONDS_PER_HOUR;
        long minutes = ((time % MILLISECONDS_PER_DAY) % MILLISECONDS_PER_HOUR) / MILLISECONDS_PER_MINUTE;
        long seconds = (((time % MILLISECONDS_PER_DAY) % MILLISECONDS_PER_HOUR) % MILLISECONDS_PER_MINUTE) / MILLISECONDS_PER_SECOND;

        StringBuilder output = new StringBuilder();

        if (days != 0L) {

            if (days == 1L) {

                output.append(days).append(" day, ");

            } else output.append(days).append(" days, ");

        }

        if (hours != 0L) {

            if (hours == 1L) {

                output.append(hours).append(" hour, ");

            } else output.append(hours).append(" hours, ");

        }

        if (minutes != 0L) {

            if (minutes == 1L) {

                output.append(minutes).append(" minute, ");

            } else output.append(minutes).append(" minutes, ");

        }

        if (seconds != 0L) {

            if (seconds == 1L) {

                output.append(seconds).append(" second");

            } else output.append(seconds).append(" seconds");

        }

        if (output.length() == 0) {

            output.append("0 seconds");

        }

        return output.toString();

    }

}
