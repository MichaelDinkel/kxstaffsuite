package com.kaixeleron.staffsuite.bungee.fetcher;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.kaixeleron.staffsuite.bungee.SuiteMain;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import javax.net.ssl.HttpsURLConnection;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class UUIDFetcher {

    private final SuiteMain m;

    private final Map<String, UUID> cache;

    public UUIDFetcher(SuiteMain m) {

        this.m = m;

        cache = Collections.synchronizedMap(new HashMap<>());

    }

    public UUID fetchUUID(String username) throws IOException {

        synchronized (cache) {

            if (cache.containsKey(username.toLowerCase())) return cache.get(username.toLowerCase());

        }

        //if someone decides to stay online for over an entire day
        for (ProxiedPlayer p : ProxyServer.getInstance().getPlayers()) {

            if (p.getName().equalsIgnoreCase(username)) {

                cache(p.getName(), p.getUniqueId());

                return p.getUniqueId();

            }

        }

        HttpsURLConnection connection = (HttpsURLConnection) new URL(String.format("https://api.mojang.com/users/profiles/minecraft/%s", username)).openConnection();

        JsonObject object = new Gson().fromJson(new InputStreamReader(connection.getInputStream()), JsonObject.class);

        if (object == null || object.get("id") == null) {

            return null;

        }

        String unformatted = object.get("id").getAsString();

        UUID out = UUID.fromString(unformatted.replaceFirst( "([0-9a-fA-F]{8})([0-9a-fA-F]{4})([0-9a-fA-F]{4})([0-9a-fA-F]{4})([0-9a-fA-F]+)", "$1-$2-$3-$4-$5" ));

        cache(username, out);

        return out;

    }

    public void cache(String username, UUID id) {

        synchronized (cache) {

            if (cache.containsValue(id)) {

                cache.values().remove(id);

            }

            cache.put(username.toLowerCase(), id);

        }

        ProxyServer.getInstance().getScheduler().schedule(m, () -> {

            synchronized (cache) {

                cache.remove(username.toLowerCase());

            }

        }, 1L, TimeUnit.DAYS);

    }

}
