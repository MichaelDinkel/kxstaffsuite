package com.kaixeleron.staffsuite.bungee.command;

import com.kaixeleron.staffsuite.bungee.SuiteMain;
import com.kaixeleron.staffsuite.bungee.database.Database;
import com.kaixeleron.staffsuite.bungee.database.DatabaseException;
import com.kaixeleron.staffsuite.bungee.database.data.Ban;
import com.kaixeleron.staffsuite.bungee.fetcher.UUIDFetcher;
import com.kaixeleron.staffsuite.time.TimeManager;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.plugin.Command;

import java.io.IOException;
import java.util.UUID;

public class CommandBanHistory extends Command {

    private final SuiteMain main;

    private final Database db;

    private final TimeManager tm;

    private final UUIDFetcher fetcher;

    private final TextComponent help, usage, noplayer, dberror, uuiderror;

    public CommandBanHistory(SuiteMain main, Database db, TimeManager tm, UUIDFetcher fetcher) {

        super("banhistory", "kxstaffsuite.command.banhistory");

        this.main = main;
        this.db = db;
        this.tm = tm;
        this.fetcher = fetcher;

        help = new TextComponent("Check a player's ban history");
        help.setColor(ChatColor.RED);

        usage = new TextComponent("Usage: ");
        usage.setColor(ChatColor.RED);

        TextComponent command = new TextComponent("/banhistory <player>");
        command.setColor(ChatColor.WHITE);
        usage.addExtra(command);

        noplayer = new TextComponent("The specified player does not exist.");
        noplayer.setColor(ChatColor.RED);

        dberror = new TextComponent("A database error has occurred.");
        dberror.setColor(ChatColor.RED);

        uuiderror = new TextComponent("An error occurred while fetching the UUID.");
        uuiderror.setColor(ChatColor.RED);

    }

    @Override
    public void execute(CommandSender sender, String[] args) {

        if (args.length == 0) {

            sender.sendMessage(help);
            sender.sendMessage(usage);

        } else {

            ProxyServer.getInstance().getScheduler().runAsync(main, () -> {

                try {

                    UUID u = fetcher.fetchUUID(args[0]);

                    if (u == null) {

                        sender.sendMessage(noplayer);

                    } else {

                        Ban[] bans = db.getBanHistory(u);

                        if (bans.length == 0) {

                            TextComponent name = new TextComponent(args[0]), nohistory = new TextComponent(" has no ban history.");
                            nohistory.setColor(ChatColor.RED);
                            name.addExtra(nohistory);

                            sender.sendMessage(name);

                        } else {

                            TextComponent history = new TextComponent("Ban history for "), name = new TextComponent(args[0]);
                            history.setColor(ChatColor.RED);
                            name.setColor(ChatColor.WHITE);
                            history.addExtra(name);

                            sender.sendMessage(history);

                            long currentTime = System.currentTimeMillis();

                            for (int i = 0; i < bans.length; i++) {

                                Ban b = bans[i];

                                TextComponent data = new TextComponent(String.format("%d", i + 1) + ". ");
                                data.setColor(ChatColor.YELLOW);

                                TextComponent placedText = new TextComponent("Placed ");
                                placedText.setColor(ChatColor.RED);
                                data.addExtra(placedText);

                                TextComponent placed = new TextComponent(tm.timeToString(currentTime - b.getPlaced()));
                                placed.setColor(ChatColor.WHITE);
                                data.addExtra(placed);

                                TextComponent ago = new TextComponent(" ago by ");
                                ago.setColor(ChatColor.RED);
                                data.addExtra(ago);

                                TextComponent by = new TextComponent(b.getBy());
                                by.setColor(ChatColor.WHITE);
                                data.addExtra(by);

                                TextComponent durationText = new TextComponent(" with duration ");
                                durationText.setColor(ChatColor.RED);
                                data.addExtra(durationText);

                                TextComponent duration = new TextComponent(b.getExpiry() == -1 ? ChatColor.RESET + "permanent" : ChatColor.RESET + tm.timeToString(b.getExpiry() - b.getPlaced()));
                                duration.setColor(ChatColor.WHITE);
                                data.addExtra(duration);

                                TextComponent reasonText = new TextComponent(" for reason ");
                                reasonText.setColor(ChatColor.RED);
                                data.addExtra(reasonText);

                                TextComponent reason = new TextComponent(b.getReason());
                                reason.setColor(ChatColor.WHITE);
                                data.addExtra(reason);

                                if (b.isExpired()) {

                                    TextComponent expiredText = new TextComponent(" expired by ");
                                    expiredText.setColor(ChatColor.RED);
                                    data.addExtra(expiredText);

                                    TextComponent expiredBy = new TextComponent(b.getExpiredBy());
                                    expiredBy.setColor(ChatColor.WHITE);
                                    data.addExtra(expiredBy);

                                } else {

                                    TextComponent active = new TextComponent("Active");
                                    active.setColor(ChatColor.GREEN);
                                    data.addExtra(active);

                                }

                                sender.sendMessage(data);

                            }

                        }

                    }

                } catch (DatabaseException e) {

                    sender.sendMessage(dberror);
                    e.printStackTrace();

                } catch (IOException e) {

                    sender.sendMessage(uuiderror);
                    e.printStackTrace();

                }

            });

        }

    }

}
