package com.kaixeleron.staffsuite.bungee.command;

import com.google.common.net.InetAddresses;
import com.kaixeleron.staffsuite.bungee.database.Database;
import com.kaixeleron.staffsuite.bungee.database.DatabaseException;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class CommandPoison extends Command {

    private final Database db;

    private final TextComponent help, usage, dberror, badip;

    public CommandPoison(Database db) {

        super("poison", "kxstaffsuite.command.poison");

        this.db = db;

        help = new TextComponent("Poison an IP address");
        help.setColor(ChatColor.RED);

        usage = new TextComponent("Usage: ");
        usage.setColor(ChatColor.RED);

        TextComponent command = new TextComponent("/poison <IP> <reason>");
        command.setColor(ChatColor.WHITE);
        usage.addExtra(command);

        dberror = new TextComponent("A database error has occurred.");
        dberror.setColor(ChatColor.RED);

        badip = new TextComponent("You must specify a valid IP address to poison.");
        badip.setColor(ChatColor.RED);

    }

    @Override
    public void execute(CommandSender sender, String[] args) {

        if (args.length < 2) {

            sender.sendMessage(help);
            sender.sendMessage(usage);

        } else {

            if (InetAddresses.isInetAddress(args[0])) {

                StringBuilder reason = new StringBuilder();

                for (int i = 1; i < args.length; i++) {

                    reason.append(args[i]).append(" ");

                }

                try {

                    if (db.getPoison(args[0].toLowerCase()) != null) {

                        TextComponent name = new TextComponent(args[0].toLowerCase());

                        TextComponent text = new TextComponent(" is already poisoned.");
                        text.setColor(ChatColor.RED);
                        name.addExtra(text);

                        sender.sendMessage(name);

                    } else {

                        db.poison(args[0].toLowerCase(), reason.substring(0, reason.length() - 1), sender.getName());

                        TextComponent ban = new TextComponent("Banned\n\nReason: ");
                        ban.setColor(ChatColor.RED);

                        TextComponent reasonText = new TextComponent(reason.toString());
                        reasonText.setColor(ChatColor.WHITE);
                        ban.addExtra(reasonText);

                        TextComponent notification = new TextComponent(sender.getName());

                        TextComponent poisoned = new TextComponent(" poisoned ");
                        poisoned.setColor(ChatColor.RED);
                        notification.addExtra(poisoned);

                        TextComponent name = new TextComponent(args[0].toLowerCase());
                        name.setColor(ChatColor.WHITE);
                        notification.addExtra(name);

                        TextComponent forNotif = new TextComponent(" for ");
                        forNotif.setColor(ChatColor.RED);
                        notification.addExtra(forNotif);

                        TextComponent reasonNotif = new TextComponent(reason.toString());
                        reasonNotif.setColor(ChatColor.WHITE);
                        notification.addExtra(reasonNotif);

                        for (ProxiedPlayer p : ProxyServer.getInstance().getPlayers()) {

                            String ip = p.getAddress().getAddress().toString().substring(1);

                            if (ip.contains(":")) ip = ip.split("[%]")[0]; //IPv6

                            if (ip.equals(args[0].toLowerCase())) {

                                db.ban(p.getUniqueId(), -1L, reason.toString(), sender.getName());

                                p.disconnect(ban);


                            } else if (p.hasPermission("kxstaffsuite.notify.poison")) {

                                p.sendMessage(notification);

                            }

                        }

                        ProxyServer.getInstance().getConsole().sendMessage(notification);

                        if (!sender.hasPermission("kxstaffsuite.notify.poison")) {

                            TextComponent noperm = new TextComponent("Poisoned ");
                            noperm.setColor(ChatColor.RED);

                            noperm.addExtra(name);
                            noperm.addExtra(forNotif);
                            noperm.addExtra(reasonNotif);

                            sender.sendMessage(noperm);

                        }

                    }

                } catch (DatabaseException e) {

                    sender.sendMessage(dberror);
                    e.printStackTrace();

                }

            } else {

                sender.sendMessage(badip);

            }

        }

    }

}
