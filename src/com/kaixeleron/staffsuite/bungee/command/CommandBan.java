package com.kaixeleron.staffsuite.bungee.command;

import com.kaixeleron.staffsuite.bungee.SuiteMain;
import com.kaixeleron.staffsuite.bungee.database.Database;
import com.kaixeleron.staffsuite.bungee.database.DatabaseException;
import com.kaixeleron.staffsuite.bungee.fetcher.UUIDFetcher;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.io.IOException;
import java.util.UUID;

public class CommandBan extends Command {

    private final SuiteMain main;

    private final Database db;

    private final UUIDFetcher fetcher;

    private final TextComponent help, usage, noplayer, dberror, uuiderror;

    public CommandBan(SuiteMain main, Database db, UUIDFetcher fetcher) {

        super("ban", "kxstaffsuite.command.ban");

        this.main = main;

        this.db = db;

        this.fetcher = fetcher;

        help = new TextComponent("Permanently ban a player");
        help.setColor(ChatColor.RED);

        usage = new TextComponent("Usage: ");
        usage.setColor(ChatColor.RED);

        TextComponent command = new TextComponent("/ban <player> <reason>");
        command.setColor(ChatColor.WHITE);
        usage.addExtra(command);

        noplayer = new TextComponent("The specified player does not exist.");
        noplayer.setColor(ChatColor.RED);

        dberror = new TextComponent("A database error has occurred.");
        dberror.setColor(ChatColor.RED);

        uuiderror = new TextComponent("An error occurred while fetching the UUID.");
        uuiderror.setColor(ChatColor.RED);

    }

    @Override
    public void execute(CommandSender sender, String[] args) {

        if (args.length < 2) {

            sender.sendMessage(help);
            sender.sendMessage(usage);

        } else {

            ProxyServer.getInstance().getScheduler().runAsync(main, () -> {

                StringBuilder reason = new StringBuilder();

                for (int i = 1; i < args.length; i++) {

                    reason.append(args[i]).append(" ");

                }

                try {

                    UUID u = fetcher.fetchUUID(args[0]);

                    if (u == null) {

                        sender.sendMessage(noplayer);

                    } else {

                        if (db.getBan(u) != null) {

                            TextComponent name = new TextComponent(args[0]);
                            TextComponent text = new TextComponent(" is already banned");
                            text.setColor(ChatColor.RED);
                            name.addExtra(text);

                            sender.sendMessage(name);

                        } else {

                            db.ban(u, -1L, reason.substring(0, reason.length() - 1), sender.getName());

                            TextComponent notification = new TextComponent(sender.getName());

                            TextComponent notifBanned = new TextComponent(" banned ");
                            notifBanned.setColor(ChatColor.RED);
                            notification.addExtra(notifBanned);

                            TextComponent target = new TextComponent(args[0]);
                            target.setColor(ChatColor.WHITE);
                            notification.addExtra(target);

                            TextComponent notifFor = new TextComponent(" for ");
                            notifFor.setColor(ChatColor.RED);
                            notification.addExtra(notifFor);

                            TextComponent notifReason = new TextComponent(reason.toString());
                            notifReason.setColor(ChatColor.WHITE);
                            notification.addExtra(notifReason);

                            for (ProxiedPlayer player : ProxyServer.getInstance().getPlayers()) {

                                if (player.getUniqueId().equals(u)) {

                                    TextComponent ban = new TextComponent("Banned\n\nReason: ");
                                    ban.setColor(ChatColor.RED);

                                    TextComponent reasonComponent = new TextComponent(reason.toString());
                                    reasonComponent.setColor(ChatColor.WHITE);
                                    ban.addExtra(reasonComponent);

                                    player.disconnect(ban);

                                } else {

                                    if (player.hasPermission("kxstaffsuite.notify.ban")) {

                                        player.sendMessage(notification);

                                    }

                                }

                            }

                            ProxyServer.getInstance().getConsole().sendMessage(notification);

                            if (!sender.hasPermission("kxstaffsuite.notify.ban")) {

                                TextComponent noperm = new TextComponent("Banned ");
                                noperm.setColor(ChatColor.RED);

                                noperm.addExtra(target);
                                noperm.addExtra(notifFor);
                                noperm.addExtra(notifReason);

                                sender.sendMessage(noperm);

                            }

                        }

                    }

                } catch (DatabaseException e) {

                    sender.sendMessage(dberror);
                    e.printStackTrace();

                } catch (IOException e) {

                    sender.sendMessage(uuiderror);
                    e.printStackTrace();

                }

            });

        }

    }

}
