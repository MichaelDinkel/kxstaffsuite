package com.kaixeleron.staffsuite.bungee.command;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class CommandStaffChat extends Command {

    private final ChatColor color;

    private final String character;

    public CommandStaffChat(ChatColor color, String character) {

        super(character, String.format("kxstaffsuite.command.staffchat.%s", character));

        this.color = color;

        this.character = character;

    }

    @Override
    public void execute(CommandSender sender, String[] args) {

        if (args.length == 0) {

            sender.sendMessage(new ComponentBuilder("Usage: ").color(ChatColor.RED).append(String.format("/%s <message>", character)).color(ChatColor.RESET).create());

        } else {

            StringBuilder message = new StringBuilder();

            for (String s : args) {

                message.append(s).append(" ");

            }

            String chatMessage = "<" + color + sender.getName() + ChatColor.RESET + "> " + color + message.toString();

            for (ProxiedPlayer p : ProxyServer.getInstance().getPlayers()) {

                if (p.hasPermission(String.format("kxstaffsuite.command.staffchat.%s", character))) {

                    p.sendMessage(TextComponent.fromLegacyText(chatMessage));

                }

            }

            ProxyServer.getInstance().getConsole().sendMessage(TextComponent.fromLegacyText(chatMessage));

        }

    }

}
