package com.kaixeleron.staffsuite.bungee.command;

import com.google.common.net.InetAddresses;
import com.kaixeleron.staffsuite.bungee.database.Database;
import com.kaixeleron.staffsuite.bungee.database.DatabaseException;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class CommandUnpoison extends Command {

    private final Database db;

    private final TextComponent help, usage, dberror, badip;

    public CommandUnpoison(Database db) {

        super("unpoison", "kxstaffsuite.command.unpoison");

        this.db = db;

        help = new TextComponent("Unpoison an IP address");
        help.setColor(ChatColor.RED);

        usage = new TextComponent("Usage: ");
        usage.setColor(ChatColor.RED);

        TextComponent command = new TextComponent("/unpoison <IP>");
        command.setColor(ChatColor.WHITE);
        usage.addExtra(command);

        dberror = new TextComponent("A database error has occurred.");
        dberror.setColor(ChatColor.RED);

        badip = new TextComponent("You must specify a valid IP address to unpoison.");
        badip.setColor(ChatColor.RED);

    }

    @Override
    public void execute(CommandSender sender, String[] args) {

        if (args.length == 0) {

            sender.sendMessage(help);
            sender.sendMessage(usage);

        } else {

            if (InetAddresses.isInetAddress(args[0])) {

                try {

                    if (db.getPoison(args[0].toLowerCase()) == null) {

                        TextComponent notpoisoned = new TextComponent(args[0]);

                        TextComponent extra = new TextComponent(" is not poisoned.");
                        extra.setColor(ChatColor.RED);
                        notpoisoned.addExtra(extra);

                        sender.sendMessage(notpoisoned);

                    } else {

                        db.unpoison(args[0].toLowerCase(), sender.getName());

                        TextComponent notification = new TextComponent(sender.getName());

                        TextComponent unpoisoned = new TextComponent(" unpoisoned ");
                        unpoisoned.setColor(ChatColor.RED);
                        notification.addExtra(unpoisoned);

                        TextComponent name = new TextComponent(args[0].toLowerCase());
                        name.setColor(ChatColor.WHITE);
                        notification.addExtra(name);

                        for (ProxiedPlayer player : ProxyServer.getInstance().getPlayers()) {

                            if (player.hasPermission("kxstaffsuite.notify.unpoison")) {

                                player.sendMessage(notification);

                            }

                        }

                        ProxyServer.getInstance().getConsole().sendMessage(notification);

                        if (!sender.hasPermission("kxstaffsuite.notify.unpoison")) {

                            TextComponent noperm = new TextComponent("Unpoisoned ");
                            noperm.setColor(ChatColor.RED);

                            noperm.addExtra(name);

                            sender.sendMessage(noperm);

                        }

                    }

                } catch (DatabaseException e) {

                    sender.sendMessage(dberror);
                    e.printStackTrace();

                }

            } else {

                sender.sendMessage(badip);

            }

        }

    }

}
