package com.kaixeleron.staffsuite.bungee.command;

import com.kaixeleron.staffsuite.bungee.SuiteMain;
import com.kaixeleron.staffsuite.bungee.database.Database;
import com.kaixeleron.staffsuite.bungee.database.DatabaseException;
import com.kaixeleron.staffsuite.bungee.database.data.Ban;
import com.kaixeleron.staffsuite.bungee.database.data.Mute;
import com.kaixeleron.staffsuite.bungee.fetcher.UUIDFetcher;
import com.kaixeleron.staffsuite.time.TimeManager;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.plugin.Command;

import java.io.IOException;
import java.util.UUID;

public class CommandCheck extends Command {

    private final SuiteMain main;

    private final Database db;

    private final TimeManager tm;

    private final UUIDFetcher fetcher;

    private final TextComponent help, usage, noplayer, dberror, uuiderror;

    public CommandCheck(SuiteMain main, Database db, TimeManager tm, UUIDFetcher fetcher) {

        super("check", "kxstaffsuite.command.check");

        this.main = main;
        this.db = db;
        this.tm = tm;
        this.fetcher = fetcher;

        help = new TextComponent("Check a player's ban or mute");
        help.setColor(ChatColor.RED);

        usage = new TextComponent("Usage: ");
        usage.setColor(ChatColor.RED);

        TextComponent command = new TextComponent("/check <player>");
        command.setColor(ChatColor.WHITE);
        usage.addExtra(command);

        noplayer = new TextComponent("The specified player does not exist.");
        noplayer.setColor(ChatColor.RED);

        dberror = new TextComponent("A database error has occurred.");
        dberror.setColor(ChatColor.RED);

        uuiderror = new TextComponent("An error occurred while fetching the UUID.");
        uuiderror.setColor(ChatColor.RED);

    }

    @Override
    public void execute(CommandSender sender, String[] args) {

        if (args.length == 0) {

            sender.sendMessage(help);
            sender.sendMessage(usage);

        } else {

            ProxyServer.getInstance().getScheduler().runAsync(main, () -> {

                try {

                    UUID u = fetcher.fetchUUID(args[0]);

                    if (u == null) {

                        sender.sendMessage(noplayer);

                    } else {

                        Mute mute = db.getMute(u);
                        Ban ban = db.getBan(u);

                        TextComponent header = new TextComponent("Moderation status for ");
                        header.setColor(ChatColor.RED);

                        TextComponent name = new TextComponent(args[0]);
                        name.setColor(ChatColor.WHITE);
                        header.addExtra(name);

                        sender.sendMessage(header);

                        TextComponent muted = new TextComponent("Muted: ");
                        muted.setColor(ChatColor.RED);

                        if (mute == null) {

                            TextComponent no = new TextComponent("no");
                            no.setColor(ChatColor.WHITE);
                            muted.addExtra(no);

                            sender.sendMessage(muted);

                        } else {

                            long expiry = mute.getExpiry();

                            TextComponent yes = new TextComponent("yes, ");
                            yes.setColor(ChatColor.WHITE);
                            muted.addExtra(yes);

                            TextComponent remaining = new TextComponent(expiry == -1L ? "permanently" : "with " + tm.timeToString(expiry - System.currentTimeMillis()) + " remaining");
                            muted.addExtra(remaining);

                            TextComponent by = new TextComponent(" by ");
                            by.setColor(ChatColor.RED);
                            muted.addExtra(by);

                            TextComponent mutedby = new TextComponent(mute.getBy());
                            mutedby.setColor(ChatColor.WHITE);
                            muted.addExtra(mutedby);

                            TextComponent forText = new TextComponent(" for ");
                            forText.setColor(ChatColor.RED);
                            muted.addExtra(forText);

                            TextComponent reason = new TextComponent(mute.getReason());
                            reason.setColor(ChatColor.WHITE);
                            muted.addExtra(reason);

                            sender.sendMessage(muted);

                        }

                        TextComponent banned = new TextComponent("Banned: ");
                        banned.setColor(ChatColor.RED);

                        if (ban == null) {

                            TextComponent no = new TextComponent("no");
                            no.setColor(ChatColor.WHITE);
                            banned.addExtra(no);

                            sender.sendMessage(banned);

                        } else {

                            long expiry = ban.getExpiry();

                            TextComponent yes = new TextComponent("yes, ");
                            yes.setColor(ChatColor.WHITE);
                            banned.addExtra(yes);

                            TextComponent remaining = new TextComponent(expiry == -1L ? "permanently" : "with " + tm.timeToString(expiry - System.currentTimeMillis()) + " remaining");
                            banned.addExtra(remaining);

                            TextComponent by = new TextComponent(" by ");
                            by.setColor(ChatColor.RED);
                            banned.addExtra(by);

                            TextComponent bannedby = new TextComponent(ban.getBy());
                            bannedby.setColor(ChatColor.WHITE);
                            banned.addExtra(bannedby);

                            TextComponent forText = new TextComponent(" for ");
                            forText.setColor(ChatColor.RED);
                            banned.addExtra(forText);

                            TextComponent reason = new TextComponent(ban.getReason());
                            reason.setColor(ChatColor.WHITE);
                            banned.addExtra(reason);

                            sender.sendMessage(banned);

                        }

                    }

                } catch (DatabaseException e) {

                    sender.sendMessage(dberror);
                    e.printStackTrace();

                } catch (IOException e) {

                    sender.sendMessage(uuiderror);
                    e.printStackTrace();

                }

            });

        }

    }

}
