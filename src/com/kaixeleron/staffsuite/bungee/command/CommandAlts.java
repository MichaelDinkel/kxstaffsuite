package com.kaixeleron.staffsuite.bungee.command;

import com.google.common.net.InetAddresses;
import com.kaixeleron.staffsuite.bungee.SuiteMain;
import com.kaixeleron.staffsuite.bungee.database.Database;
import com.kaixeleron.staffsuite.bungee.database.DatabaseException;
import com.kaixeleron.staffsuite.bungee.database.data.Alt;
import com.kaixeleron.staffsuite.bungee.fetcher.UUIDFetcher;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.plugin.Command;

import java.io.IOException;
import java.util.Calendar;
import java.util.UUID;

public class CommandAlts extends Command {

    private final SuiteMain main;

    private final Database db;

    private final UUIDFetcher fetcher;

    private final TextComponent description, descriptionip, usage, usageip, noplayer, dberror, uuiderror;

    public CommandAlts(SuiteMain main, Database db, UUIDFetcher fetcher) {

        super("alts", "kxstaffsuite.command.alts");

        this.main = main;

        this.db = db;

        this.fetcher = fetcher;

        description = new TextComponent("Check for alts by name");
        description.setColor(ChatColor.RED);

        descriptionip = new TextComponent("Check for alts by name or IP address");
        descriptionip.setColor(ChatColor.RED);

        usage = new TextComponent("Usage: ");
        usage.setColor(ChatColor.RED);

        TextComponent command = new TextComponent("/alts <player>");
        command.setColor(ChatColor.WHITE);
        usage.addExtra(command);

        usageip = new TextComponent("Usage: ");
        usageip.setColor(ChatColor.RED);

        TextComponent commandip = new TextComponent("/alts <player|IP>");
        commandip.setColor(ChatColor.WHITE);
        usageip.addExtra(commandip);

        noplayer = new TextComponent("The specified player does not exist.");
        noplayer.setColor(ChatColor.RED);

        dberror = new TextComponent("A database error has occurred.");
        dberror.setColor(ChatColor.RED);

        uuiderror = new TextComponent("An error occurred while fetching the UUID.");
        uuiderror.setColor(ChatColor.RED);

    }

    @SuppressWarnings("UnstableApiUsage")
    @Override
    public void execute(CommandSender sender, String[] args) {

        if (args.length == 0) {

            if (sender.hasPermission("kxstaffsuite.command.alts.ip")) {

                sender.sendMessage(descriptionip);
                sender.sendMessage(usageip);

            } else {

                sender.sendMessage(description);
                sender.sendMessage(usage);

            }

        } else {

            if (InetAddresses.isInetAddress(args[0]) && sender.hasPermission("kxstaffsuite.command.alts.ip")) {

                try {

                    Alt[] alts = db.getAlts(args[0]);

                    if (alts.length > 0) {

                        TextComponent accounts = new TextComponent("Accounts on IP ");
                        accounts.setColor(ChatColor.RED);

                        TextComponent ip = new TextComponent(args[0]);
                        ip.setColor(ChatColor.WHITE);
                        accounts.addExtra(ip);

                        sender.sendMessage(accounts);

                        showAlts(sender, alts);

                    } else {

                        TextComponent noaccounts = new TextComponent("No accounts found on IP ");
                        noaccounts.setColor(ChatColor.RED);

                        TextComponent ip = new TextComponent(args[0]);
                        ip.setColor(ChatColor.WHITE);
                        noaccounts.addExtra(ip);

                        sender.sendMessage(noaccounts);

                    }

                } catch (DatabaseException e) {

                    sender.sendMessage(dberror);
                    e.printStackTrace();

                }

            } else {

                ProxyServer.getInstance().getScheduler().runAsync(main, () -> {

                    try {

                        UUID u = fetcher.fetchUUID(args[0]);

                        if (u == null) {

                            sender.sendMessage(noplayer);

                        } else {

                            Alt[] alts = db.getAlts(u);

                            if (alts.length > 0) {

                                if (sender.hasPermission("kxstaffsuite.command.alts.ip")) {

                                    TextComponent prefix = new TextComponent("IP address: ");
                                    prefix.setColor(ChatColor.RED);

                                    TextComponent ip = new TextComponent(alts[0].getIp());
                                    ip.setColor(ChatColor.WHITE);
                                    prefix.addExtra(ip);

                                    sender.sendMessage(prefix);

                                }

                                TextComponent accounts = new TextComponent("Accounts for player ");
                                accounts.setColor(ChatColor.RED);

                                TextComponent player = new TextComponent(args[0]);
                                player.setColor(ChatColor.WHITE);
                                accounts.addExtra(player);

                                sender.sendMessage(accounts);

                                showAlts(sender, alts);

                            } else {

                                TextComponent noaccounts = new TextComponent("No accounts found for player ");
                                noaccounts.setColor(ChatColor.RED);

                                TextComponent player = new TextComponent(args[0]);
                                player.setColor(ChatColor.WHITE);
                                noaccounts.addExtra(player);

                                sender.sendMessage(noaccounts);

                            }

                        }

                    } catch (DatabaseException e) {

                        sender.sendMessage(dberror);
                        e.printStackTrace();

                    } catch (IOException e) {

                        sender.sendMessage(uuiderror);
                        e.printStackTrace();

                    }

                });

            }

        }

    }

    private void showAlts(CommandSender sender, Alt[] alts) {

        for (Alt a : alts) {

            TextComponent name = new TextComponent(a.getName());
            name.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(a.getId().toString()).create()));
            TextComponent logintext = new TextComponent(" last login ");
            logintext.setColor(net.md_5.bungee.api.ChatColor.RED);

            Calendar c = Calendar.getInstance();
            c.setTimeInMillis(a.getLastLogin());
            TextComponent login = new TextComponent(String.format("%d/%d/%d", c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH) + 1, c.get(Calendar.YEAR)));
            login.setColor(net.md_5.bungee.api.ChatColor.RESET);

            name.addExtra(logintext);
            name.addExtra(login);

            sender.sendMessage(name);

        }

    }

}
