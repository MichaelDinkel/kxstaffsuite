package com.kaixeleron.staffsuite.bungee.command;

import com.kaixeleron.staffsuite.bungee.database.Database;
import com.kaixeleron.staffsuite.bungee.database.DatabaseException;
import com.kaixeleron.staffsuite.bungee.database.data.Poison;
import com.kaixeleron.staffsuite.time.TimeManager;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.plugin.Command;

public class CommandCheckPoison extends Command {

    private final Database db;

    private final TimeManager tm;

    private final TextComponent help, usage, historyText, dberror;

    public CommandCheckPoison(Database db, TimeManager tm) {

        super("checkpoison", "kxstaffsuite.command.checkpoison");

        this.db = db;
        this.tm = tm;

        help = new TextComponent("Check the poison status and history of an IP address");
        help.setColor(ChatColor.RED);

        usage = new TextComponent("Usage: ");
        usage.setColor(ChatColor.RED);

        TextComponent command = new TextComponent("/checkpoison <IP>");
        command.setColor(ChatColor.WHITE);
        usage.addExtra(command);

        historyText = new TextComponent("Poison history:");
        historyText.setColor(ChatColor.RED);

        dberror = new TextComponent("A database error has occurred.");
        dberror.setColor(ChatColor.RED);

    }

    @Override
    public void execute(CommandSender sender, String[] args) {

        if (args.length == 0) {

            sender.sendMessage(help);
            sender.sendMessage(usage);

        } else {

            try {

                Poison poison = db.getPoison(args[0].toLowerCase());
                Poison[] history = db.getPoisonHistory(args[0].toLowerCase());

                TextComponent poisoned = new TextComponent(args[0].toLowerCase());

                if (poison == null) {

                    TextComponent extra = new TextComponent(" is not poisoned.");
                    extra.setColor(ChatColor.RED);
                    poisoned.addExtra(extra);

                    sender.sendMessage(poisoned);

                } else {

                    TextComponent extra = new TextComponent(" has been poisoned by ");
                    extra.setColor(ChatColor.RED);
                    poisoned.addExtra(extra);

                    TextComponent by = new TextComponent(poison.getBy());
                    by.setColor(ChatColor.WHITE);
                    poisoned.addExtra(by);

                    TextComponent forText = new TextComponent(" for ");
                    forText.setColor(ChatColor.RED);
                    poisoned.addExtra(forText);

                    TextComponent reason = new TextComponent(poison.getReason());
                    reason.setColor(ChatColor.WHITE);
                    poisoned.addExtra(reason);

                    sender.sendMessage(poisoned);

                }

                if (history.length > 0) {

                    sender.sendMessage(historyText);

                    for (int i = 0; i < history.length; i++) {

                        Poison p = history[i];

                        TextComponent message = new TextComponent(String.format("%d. ", i + 1));
                        message.setColor(ChatColor.YELLOW);

                        TextComponent placedText = new TextComponent("Placed ");
                        placedText.setColor(ChatColor.RED);
                        message.addExtra(placedText);

                        TextComponent placed = new TextComponent(tm.timeToString(System.currentTimeMillis() - p.getPlaced()));
                        placed.setColor(ChatColor.WHITE);
                        message.addExtra(placed);

                        TextComponent ago = new TextComponent(" ago by ");
                        ago.setColor(ChatColor.RED);
                        message.addExtra(ago);

                        TextComponent by = new TextComponent(p.getBy());
                        by.setColor(ChatColor.WHITE);
                        message.addExtra(by);

                        TextComponent reasonText = new TextComponent(" for reason ");
                        reasonText.setColor(ChatColor.RED);
                        message.addExtra(reasonText);

                        TextComponent reason = new TextComponent(p.getReason());
                        reason.setColor(ChatColor.WHITE);
                        message.addExtra(reason);

                        TextComponent expiredText = new TextComponent(" expired by ");
                        expiredText.setColor(ChatColor.RED);
                        message.addExtra(expiredText);

                        TextComponent expiredBy = new TextComponent(p.getExpiredBy());
                        expiredBy.setColor(ChatColor.WHITE);
                        message.addExtra(expiredBy);

                        sender.sendMessage(message);

                    }

                }

            } catch (DatabaseException e) {

                sender.sendMessage(dberror);
                e.printStackTrace();

            }

        }

    }

}
