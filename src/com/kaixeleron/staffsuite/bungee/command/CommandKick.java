package com.kaixeleron.staffsuite.bungee.command;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class CommandKick extends Command {

    private final TextComponent help, usage, noplayer;

    public CommandKick() {

        super("kick", "kxstaffsuite.command.kick");

        help = new TextComponent("Kick a player");
        help.setColor(ChatColor.RED);

        usage = new TextComponent("Usage: ");
        usage.setColor(ChatColor.RED);

        TextComponent command = new TextComponent("/kick <player> <reason>");
        command.setColor(ChatColor.WHITE);
        usage.addExtra(command);

        noplayer = new TextComponent("Player not found.");
        noplayer.setColor(ChatColor.RED);

    }

    @Override
    public void execute(CommandSender sender, String[] args) {

        if (args.length < 2) {

            sender.sendMessage(help);
            sender.sendMessage(usage);

        } else {

            ProxiedPlayer p = ProxyServer.getInstance().getPlayer(args[0]);

            if (p == null) {

                sender.sendMessage(noplayer);

            } else {

                StringBuilder reason = new StringBuilder();

                for (int i = 1; i < args.length; i++) {

                    reason.append(args[i]).append(" ");

                }

                TextComponent kicked = new TextComponent("Kicked\n\nReason: ");
                kicked.setColor(ChatColor.RED);

                TextComponent reasonComp = new TextComponent(reason.toString());
                reasonComp.setColor(ChatColor.WHITE);
                kicked.addExtra(reasonComp);

                p.disconnect(kicked);

                TextComponent notification = new TextComponent(sender.getName());

                TextComponent kickedNotif = new TextComponent(" kicked ");
                kickedNotif.setColor(ChatColor.RED);
                notification.addExtra(kickedNotif);

                TextComponent name = new TextComponent(p.getName());
                name.setColor(ChatColor.WHITE);
                notification.addExtra(name);

                TextComponent forNotif = new TextComponent(" for ");
                forNotif.setColor(ChatColor.RED);
                notification.addExtra(forNotif);

                TextComponent reasonNotif = new TextComponent(reason.toString());
                reasonNotif.setColor(ChatColor.WHITE);
                notification.addExtra(reasonNotif);

                for (ProxiedPlayer player : ProxyServer.getInstance().getPlayers()) {

                    if (player.hasPermission("kxstaffsuite.notify.kick")) {

                        player.sendMessage(notification);

                    }

                }

                ProxyServer.getInstance().getConsole().sendMessage(notification);

                if (!sender.hasPermission("kxstaffsuite.notify.kick")) {

                    TextComponent noperm = new TextComponent("Kicked ");
                    noperm.setColor(ChatColor.RED);
                    noperm.addExtra(name);
                    noperm.addExtra(forNotif);
                    noperm.addExtra(reasonNotif);

                    sender.sendMessage(noperm);

                }

            }

        }

    }

}
