package com.kaixeleron.staffsuite.bungee.command;

import com.kaixeleron.staffsuite.bungee.SuiteMain;
import com.kaixeleron.staffsuite.bungee.database.Database;
import com.kaixeleron.staffsuite.bungee.database.DatabaseException;
import com.kaixeleron.staffsuite.bungee.database.data.Mute;
import com.kaixeleron.staffsuite.bungee.fetcher.UUIDFetcher;
import com.kaixeleron.staffsuite.time.TimeManager;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.plugin.Command;

import java.io.IOException;
import java.util.UUID;

public class CommandMuteHistory extends Command {

    private final SuiteMain main;

    private final Database db;

    private final TimeManager tm;

    private final UUIDFetcher fetcher;

    private final TextComponent help, usage, noplayer, dberror, uuiderror;

    public CommandMuteHistory(SuiteMain main, Database db, TimeManager tm, UUIDFetcher fetcher) {

        super("mutehistory", "kxstaffsuite.command.mutehistory");

        this.main = main;
        this.db = db;
        this.tm = tm;
        this.fetcher = fetcher;

        help = new TextComponent("Check a player's mute history");
        help.setColor(ChatColor.RED);

        usage = new TextComponent("Usage: ");
        usage.setColor(ChatColor.RED);

        TextComponent command = new TextComponent("/mutehistory <player>");
        command.setColor(ChatColor.WHITE);
        usage.addExtra(command);

        noplayer = new TextComponent("The specified player does not exist.");
        noplayer.setColor(ChatColor.RED);

        dberror = new TextComponent("A database error has occurred.");
        dberror.setColor(ChatColor.RED);

        uuiderror = new TextComponent("An error occurred while fetching the UUID.");
        uuiderror.setColor(ChatColor.RED);

    }

    @Override
    public void execute(CommandSender sender, String[] args) {

        if (args.length == 0) {

            sender.sendMessage(help);
            sender.sendMessage(usage);

        } else {

            ProxyServer.getInstance().getScheduler().runAsync(main, () -> {

                try {

                    UUID u = fetcher.fetchUUID(args[0]);

                    if (u == null) {

                        sender.sendMessage(noplayer);

                    } else {

                        Mute[] mutes = db.getMuteHistory(u);

                        if (mutes.length == 0) {

                            TextComponent nohistory = new TextComponent(args[0]);

                            TextComponent extra = new TextComponent(" has no mute history.");
                            extra.setColor(ChatColor.RED);
                            nohistory.addExtra(extra);

                            sender.sendMessage(nohistory);

                        } else {

                            TextComponent header = new TextComponent("Mute history for ");
                            header.setColor(ChatColor.RED);

                            TextComponent name = new TextComponent(args[0]);
                            name.setColor(ChatColor.WHITE);
                            header.addExtra(name);

                            sender.sendMessage(header);

                            long currentTime = System.currentTimeMillis();

                            for (int i = 0; i < mutes.length; i++) {

                                Mute m = mutes[i];

                                TextComponent data = new TextComponent(String.format("%d", i + 1) + ". ");
                                data.setColor(ChatColor.YELLOW);

                                TextComponent placedText = new TextComponent("Placed ");
                                placedText.setColor(ChatColor.RED);
                                data.addExtra(placedText);

                                TextComponent placed = new TextComponent(tm.timeToString(currentTime - m.getPlaced()));
                                placed.setColor(ChatColor.WHITE);
                                data.addExtra(placed);

                                TextComponent ago = new TextComponent(" ago by ");
                                ago.setColor(ChatColor.RED);
                                data.addExtra(ago);

                                TextComponent by = new TextComponent(m.getBy());
                                by.setColor(ChatColor.WHITE);
                                data.addExtra(by);

                                TextComponent durationText = new TextComponent(" with duration ");
                                durationText.setColor(ChatColor.RED);
                                data.addExtra(durationText);

                                TextComponent duration = new TextComponent(m.getExpiry() == -1 ? ChatColor.RESET + "permanent" : ChatColor.RESET + tm.timeToString(m.getExpiry() - m.getPlaced()));
                                duration.setColor(ChatColor.WHITE);
                                data.addExtra(duration);

                                TextComponent reasonText = new TextComponent(" for reason ");
                                reasonText.setColor(ChatColor.RED);
                                data.addExtra(reasonText);

                                TextComponent reason = new TextComponent(m.getReason());
                                reason.setColor(ChatColor.WHITE);
                                data.addExtra(reason);

                                if (m.isExpired()) {

                                    TextComponent expiredText = new TextComponent(" expired by ");
                                    expiredText.setColor(ChatColor.RED);
                                    data.addExtra(expiredText);

                                    TextComponent expiredBy = new TextComponent(m.getExpiredBy());
                                    expiredBy.setColor(ChatColor.WHITE);
                                    data.addExtra(expiredBy);

                                } else {

                                    TextComponent active = new TextComponent("Active");
                                    active.setColor(ChatColor.GREEN);
                                    data.addExtra(active);

                                }

                                sender.sendMessage(data);

                            }

                        }

                    }

                } catch (DatabaseException e) {

                    sender.sendMessage(dberror);
                    e.printStackTrace();

                } catch (IOException e) {

                    sender.sendMessage(uuiderror);
                    e.printStackTrace();

                }

            });

        }

    }

}
