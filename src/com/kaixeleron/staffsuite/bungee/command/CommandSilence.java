package com.kaixeleron.staffsuite.bungee.command;

import com.kaixeleron.staffsuite.bungee.listener.ChatListener;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.plugin.Command;

public class CommandSilence extends Command {

    private final ChatListener listener;

    private final TextComponent unsilenced, silenced;

    public CommandSilence(ChatListener listener) {

        super("silence", "kxstaffsuite.command.silence");

        this.listener = listener;

        unsilenced = new TextComponent("The server is no longer silenced.");
        unsilenced.setColor(ChatColor.RED);

        silenced = new TextComponent("The server is now silenced.");
        silenced.setColor(ChatColor.RED);

    }

    @Override
    public void execute(CommandSender sender, String[] args) {

        if (listener.isSilenced()) {

            ProxyServer.getInstance().broadcast(unsilenced);
            listener.setSilenced(false);

        } else {

            ProxyServer.getInstance().broadcast(silenced);
            listener.setSilenced(true);

        }

    }

}
