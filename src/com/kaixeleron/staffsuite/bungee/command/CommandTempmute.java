package com.kaixeleron.staffsuite.bungee.command;

import com.kaixeleron.staffsuite.bungee.SuiteMain;
import com.kaixeleron.staffsuite.bungee.database.Database;
import com.kaixeleron.staffsuite.bungee.database.DatabaseException;
import com.kaixeleron.staffsuite.bungee.fetcher.UUIDFetcher;
import com.kaixeleron.staffsuite.time.TimeManager;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.io.IOException;
import java.util.UUID;

public class CommandTempmute extends Command {

    private final SuiteMain main;

    private final Database db;

    private final TimeManager timeManager;

    private final UUIDFetcher fetcher;

    private final TextComponent help, usage, noplayer, pnotif, dberror, uuiderror, formaterror;

    public CommandTempmute(SuiteMain main, Database db, TimeManager timeManager, UUIDFetcher fetcher) {

        super("tempmute", "kxstaffsuite.command.tempmute");

        this.main = main;

        this.db = db;

        this.timeManager = timeManager;

        this.fetcher = fetcher;

        help = new TextComponent("Temporarily mute a player");
        help.setColor(ChatColor.RED);

        usage = new TextComponent("Usage: ");
        usage.setColor(ChatColor.RED);

        TextComponent command = new TextComponent("/tempmute <player> <time> <reason>");
        command.setColor(ChatColor.WHITE);
        usage.addExtra(command);

        noplayer = new TextComponent("The specified player does not exist.");
        noplayer.setColor(ChatColor.RED);

        pnotif = new TextComponent("You have been temporarily muted.");
        pnotif.setColor(ChatColor.RED);

        dberror = new TextComponent("A database error has occurred.");
        dberror.setColor(ChatColor.RED);

        uuiderror = new TextComponent("An error occurred while fetching the UUID.");
        uuiderror.setColor(ChatColor.RED);

        formaterror = new TextComponent("Invalid time. Must be formatted similar to ");
        formaterror.setColor(ChatColor.RED);

        TextComponent example = new TextComponent("1h1m1s");
        example.setColor(ChatColor.WHITE);
        formaterror.addExtra(example);

        TextComponent period = new TextComponent(".");
        period.setColor(ChatColor.RED);
        formaterror.addExtra(period);

    }

    @Override
    public void execute(CommandSender sender, String[] args) {

        if (args.length < 3) {

            sender.sendMessage(help);
            sender.sendMessage(usage);

        } else {

            ProxyServer.getInstance().getScheduler().runAsync(main, () -> {

                try {

                    String timeStr = args[1].toLowerCase();

                    long time = timeManager.parseTime(timeStr) + System.currentTimeMillis();

                    StringBuilder reason = new StringBuilder();

                    for (int i = 2; i < args.length; i++) {

                        reason.append(args[i]).append(" ");

                    }

                    try {

                        UUID u = fetcher.fetchUUID(args[0]);

                        if (u == null) {

                            sender.sendMessage(noplayer);

                        } else {

                            if (db.getMute(u) != null) {

                                TextComponent already = new TextComponent(args[0]);

                                TextComponent text = new TextComponent(" is already muted.");
                                text.setColor(ChatColor.RED);
                                already.addExtra(text);

                                sender.sendMessage(already);

                            } else {

                                db.mute(u, time, reason.substring(0, reason.length() - 1), sender.getName());

                                TextComponent notification = new TextComponent(sender.getName());

                                TextComponent tempMuted = new TextComponent(" temporarily muted ");
                                tempMuted.setColor(ChatColor.RED);

                                TextComponent name = new TextComponent(args[0]);
                                name.setColor(ChatColor.WHITE);

                                TextComponent forNotif = new TextComponent(" for ");
                                forNotif.setColor(ChatColor.RED);

                                TextComponent timeNotif = new TextComponent(timeStr);
                                timeNotif.setColor(ChatColor.WHITE);

                                TextComponent reasonNotif = new TextComponent(reason.toString());
                                reasonNotif.setColor(ChatColor.WHITE);

                                notification.addExtra(tempMuted);
                                notification.addExtra(name);
                                notification.addExtra(forNotif);
                                notification.addExtra(timeNotif);
                                notification.addExtra(forNotif);
                                notification.addExtra(reasonNotif);

                                for (ProxiedPlayer p : ProxyServer.getInstance().getPlayers()) {

                                    if (p.getUniqueId().equals(u)) {

                                        TextComponent duration = new TextComponent("Duration: ");
                                        duration.setColor(ChatColor.RED);

                                        TextComponent durationString = new TextComponent(timeManager.timeToString(time - System.currentTimeMillis()));
                                        durationString.setColor(ChatColor.WHITE);
                                        duration.addExtra(durationString);

                                        TextComponent preason = new TextComponent("Reason: ");
                                        preason.setColor(ChatColor.RED);

                                        TextComponent reasonString = new TextComponent(reason.toString());
                                        reasonString.setColor(ChatColor.WHITE);
                                        preason.addExtra(reasonString);

                                        p.sendMessage(pnotif);
                                        p.sendMessage(duration);
                                        p.sendMessage(preason);

                                    } else if (p.hasPermission("kxstaffsuite.notify.tempmute")) {

                                        p.sendMessage(notification);

                                    }

                                }

                                ProxyServer.getInstance().getConsole().sendMessage(notification);

                                if (!sender.hasPermission("kxstaffsuite.notify.tempmute")) {

                                    TextComponent noperm = new TextComponent("Temporarily muted ");
                                    noperm.setColor(ChatColor.RED);

                                    noperm.addExtra(name);
                                    noperm.addExtra(forNotif);
                                    noperm.addExtra(timeNotif);
                                    noperm.addExtra(forNotif);
                                    noperm.addExtra(reasonNotif);

                                    sender.sendMessage(noperm);

                                }

                            }

                        }

                    } catch (DatabaseException e) {

                        sender.sendMessage(dberror);
                        e.printStackTrace();

                    } catch (IOException e) {

                        sender.sendMessage(uuiderror);
                        e.printStackTrace();

                    }

                } catch (IllegalArgumentException e) {

                    sender.sendMessage(formaterror);

                }

            });

        }

    }

}
