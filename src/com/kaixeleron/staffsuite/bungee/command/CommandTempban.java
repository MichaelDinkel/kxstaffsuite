package com.kaixeleron.staffsuite.bungee.command;

import com.kaixeleron.staffsuite.bungee.SuiteMain;
import com.kaixeleron.staffsuite.bungee.database.Database;
import com.kaixeleron.staffsuite.bungee.database.DatabaseException;
import com.kaixeleron.staffsuite.bungee.fetcher.UUIDFetcher;
import com.kaixeleron.staffsuite.time.TimeManager;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.io.IOException;
import java.util.UUID;

public class CommandTempban extends Command {

    private final SuiteMain main;

    private final Database db;

    private final TimeManager timeManager;

    private final UUIDFetcher fetcher;

    private final TextComponent help, usage, noplayer, dberror, uuiderror, formaterror;

    public CommandTempban(SuiteMain main, Database db, TimeManager timeManager, UUIDFetcher fetcher) {

        super("tempban", "kxstaffsuite.command.tempban");

        this.main = main;

        this.db = db;

        this.timeManager = timeManager;

        this.fetcher = fetcher;

        help = new TextComponent("Temporarily ban a player");
        help.setColor(ChatColor.RED);

        usage = new TextComponent("Usage: ");
        usage.setColor(ChatColor.RED);

        TextComponent command = new TextComponent("/tempban <player> <time> <reason>");
        command.setColor(ChatColor.WHITE);
        usage.addExtra(command);

        noplayer = new TextComponent("The specified player does not exist.");
        noplayer.setColor(ChatColor.RED);

        dberror = new TextComponent("A database error has occurred.");
        dberror.setColor(ChatColor.RED);

        uuiderror = new TextComponent("An error occurred while fetching the UUID.");
        uuiderror.setColor(ChatColor.RED);

        formaterror = new TextComponent("Invalid time. Must be formatted similar to ");
        formaterror.setColor(ChatColor.RED);

        TextComponent example = new TextComponent("1h1m1s");
        example.setColor(ChatColor.WHITE);
        formaterror.addExtra(example);

        TextComponent period = new TextComponent(".");
        period.setColor(ChatColor.RED);
        formaterror.addExtra(period);

    }

    @Override
    public void execute(CommandSender sender, String[] args) {

        if (args.length < 3) {

            sender.sendMessage(help);
            sender.sendMessage(usage);

        } else {

            ProxyServer.getInstance().getScheduler().runAsync(main, () -> {

                try {

                    String timeStr = args[1].toLowerCase();

                    long time = timeManager.parseTime(timeStr) + System.currentTimeMillis();

                    StringBuilder reason = new StringBuilder();

                    for (int i = 2; i < args.length; i++) {

                        reason.append(args[i]).append(" ");

                    }

                    try {

                        UUID u = fetcher.fetchUUID(args[0]);

                        if (u == null) {

                            sender.sendMessage(noplayer);

                        } else {

                            if (db.getBan(u) != null) {

                                TextComponent already = new TextComponent(args[0]);

                                TextComponent message = new TextComponent(" is already banned.");
                                message.setColor(ChatColor.RED);
                                already.addExtra(message);

                                sender.sendMessage(already);

                            } else {

                                db.ban(u, time, reason.substring(0, reason.length() - 1), sender.getName());

                                TextComponent notification = new TextComponent(sender.getName());

                                TextComponent tempbanned = new TextComponent(" temporarily banned ");
                                tempbanned.setColor(ChatColor.RED);
                                notification.addExtra(tempbanned);

                                TextComponent name = new TextComponent(args[0]);
                                name.setColor(ChatColor.WHITE);
                                notification.addExtra(name);

                                TextComponent forNotif = new TextComponent(" for ");
                                forNotif.setColor(ChatColor.RED);
                                notification.addExtra(forNotif);

                                TextComponent timeStrNotif = new TextComponent(timeStr);
                                timeStrNotif.setColor(ChatColor.WHITE);
                                notification.addExtra(timeStrNotif);

                                notification.addExtra(forNotif);

                                TextComponent reasonText = new TextComponent(reason.toString());
                                reasonText.setColor(ChatColor.WHITE);
                                notification.addExtra(reasonText);

                                for (ProxiedPlayer p : ProxyServer.getInstance().getPlayers()) {

                                    if (p.getUniqueId().equals(u)) {

                                        TextComponent disconnect = new TextComponent("Temporarily banned for ");
                                        disconnect.setColor(ChatColor.RED);

                                        TextComponent timeComp = new TextComponent(timeManager.timeToString(time - System.currentTimeMillis()));
                                        timeComp.setColor(ChatColor.WHITE);
                                        disconnect.addExtra(timeComp);

                                        TextComponent reasonPrefix = new TextComponent("\n\nReason: ");
                                        reasonPrefix.setColor(ChatColor.RED);
                                        disconnect.addExtra(reasonPrefix);

                                        disconnect.addExtra(reasonText);

                                        p.disconnect(disconnect);

                                    } else if (p.hasPermission("kxstaffsuite.notify.tempban")) {

                                        p.sendMessage(notification);

                                    }

                                }

                                ProxyServer.getInstance().getConsole().sendMessage(notification);

                                if (!sender.hasPermission("kxstaffsuite.notify.tempban")) {

                                    TextComponent noperm = new TextComponent("Temporarily banned ");
                                    noperm.setColor(ChatColor.RED);

                                    noperm.addExtra(name);
                                    noperm.addExtra(forNotif);
                                    noperm.addExtra(timeStrNotif);
                                    noperm.addExtra(forNotif);
                                    noperm.addExtra(reasonText);

                                    sender.sendMessage(noperm);

                                }

                            }

                        }

                    } catch (DatabaseException e) {

                        sender.sendMessage(dberror);
                        e.printStackTrace();

                    } catch (IOException e) {

                        sender.sendMessage(uuiderror);
                        e.printStackTrace();

                    }

                } catch (IllegalArgumentException e) {

                    sender.sendMessage(formaterror);

                }

            });

        }

    }

}
