package com.kaixeleron.staffsuite.bungee.command;

import com.kaixeleron.staffsuite.bungee.SuiteMain;
import com.kaixeleron.staffsuite.bungee.database.Database;
import com.kaixeleron.staffsuite.bungee.database.DatabaseException;
import com.kaixeleron.staffsuite.bungee.fetcher.UUIDFetcher;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.io.IOException;
import java.util.UUID;

public class CommandMute extends Command {

    private final SuiteMain main;

    private final Database db;

    private final UUIDFetcher fetcher;

    private final TextComponent help, usage, noplayer, pnotif, dberror, uuiderror;

    public CommandMute(SuiteMain main, Database db, UUIDFetcher fetcher) {

        super("mute", "kxstaffsuite.command.mute");

        this.main = main;
        this.db = db;
        this.fetcher = fetcher;

        help = new TextComponent("Permanently mute a player");
        help.setColor(ChatColor.RED);

        usage = new TextComponent("Usage: ");
        usage.setColor(ChatColor.RED);

        TextComponent command = new TextComponent("/mute <player> <reason>");
        command.setColor(ChatColor.WHITE);
        usage.addExtra(command);

        noplayer = new TextComponent("The specified player does not exist.");
        noplayer.setColor(ChatColor.RED);

        pnotif = new TextComponent("You have been permanently muted.");
        pnotif.setColor(ChatColor.RED);

        dberror = new TextComponent("A database error has occurred.");
        dberror.setColor(ChatColor.RED);

        uuiderror = new TextComponent("An error occurred while fetching the UUID.");
        usage.setColor(ChatColor.RED);

    }

    @Override
    public void execute(CommandSender sender, String[] args) {

        if (args.length < 2) {

            sender.sendMessage(help);
            sender.sendMessage(usage);

        } else {

            StringBuilder reason = new StringBuilder();

            for (int i = 1; i < args.length; i++) {

                reason.append(args[i]).append(" ");

            }

            ProxyServer.getInstance().getScheduler().runAsync(main, () -> {

                try {

                    UUID u = fetcher.fetchUUID(args[0]);

                    if (u == null) {

                        sender.sendMessage(noplayer);

                    } else {

                        if (db.getMute(u) != null) {

                            TextComponent name = new TextComponent(args[0]);

                            TextComponent already = new TextComponent(" is already muted.");
                            already.setColor(ChatColor.RED);
                            name.addExtra(already);

                            sender.sendMessage(name);

                        } else {

                            db.mute(u, -1L, reason.substring(0, reason.length() - 1), sender.getName());

                            TextComponent notification = new TextComponent(sender.getName());

                            TextComponent muted = new TextComponent(" muted ");
                            muted.setColor(ChatColor.RED);
                            notification.addExtra(muted);

                            TextComponent name = new TextComponent(args[0]);
                            name.setColor(ChatColor.WHITE);
                            notification.addExtra(name);

                            TextComponent forNotif = new TextComponent(" for ");
                            forNotif.setColor(ChatColor.RED);
                            notification.addExtra(forNotif);

                            TextComponent sreasonNotif = new TextComponent(reason.toString());
                            sreasonNotif.setColor(ChatColor.WHITE);
                            notification.addExtra(sreasonNotif);

                            for (ProxiedPlayer player : ProxyServer.getInstance().getPlayers()) {

                                if (player.getUniqueId().equals(u)) {

                                    TextComponent reasonNotif = new TextComponent("Reason: ");
                                    reasonNotif.setColor(ChatColor.RED);

                                    TextComponent reasonText = new TextComponent(reason.toString());
                                    reasonText.setColor(ChatColor.WHITE);
                                    reasonNotif.addExtra(reasonText);

                                    player.sendMessage(pnotif);
                                    player.sendMessage(reasonNotif);

                                }

                                if (player.hasPermission("kxstaffsuite.notify.mute")) {

                                    player.sendMessage(notification);

                                }

                            }

                            ProxyServer.getInstance().getConsole().sendMessage(notification);

                            if (!sender.hasPermission("kxstaffsuite.notify.mute")) {

                                TextComponent noperm = new TextComponent("Muted ");
                                noperm.setColor(ChatColor.RED);
                                noperm.addExtra(name);
                                noperm.addExtra(forNotif);
                                noperm.addExtra(sreasonNotif);

                                sender.sendMessage(noperm);

                            }

                        }

                    }

                } catch (DatabaseException e) {

                    sender.sendMessage(dberror);
                    e.printStackTrace();

                } catch (IOException e) {

                    sender.sendMessage(uuiderror);
                    e.printStackTrace();

                }

            });

        }

    }

}
