package com.kaixeleron.staffsuite.bungee.command;

import com.kaixeleron.staffsuite.bungee.SuiteMain;
import com.kaixeleron.staffsuite.bungee.database.Database;
import com.kaixeleron.staffsuite.bungee.database.DatabaseException;
import com.kaixeleron.staffsuite.bungee.fetcher.UUIDFetcher;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.io.IOException;
import java.util.UUID;

public class CommandUnban extends Command {

    private final SuiteMain main;

    private final Database db;

    private final UUIDFetcher fetcher;

    private final TextComponent help, usage, noplayer, dberror, uuiderror;

    public CommandUnban(SuiteMain main, Database db, UUIDFetcher fetcher) {

        super("unban", "kxstaffsuite.command.unban");

        this.main = main;

        this.db = db;
        this.fetcher = fetcher;

        help = new TextComponent("Unban a player");
        help.setColor(ChatColor.RED);

        usage = new TextComponent("Usage: ");
        usage.setColor(ChatColor.RED);

        TextComponent command = new TextComponent("/unban <player>");
        command.setColor(ChatColor.WHITE);
        usage.addExtra(command);

        noplayer = new TextComponent("The specified player does not exist.");
        noplayer.setColor(ChatColor.RED);

        dberror = new TextComponent("A database error has occurred.");
        dberror.setColor(ChatColor.RED);

        uuiderror = new TextComponent("An error occurred while fetching the UUID.");
        uuiderror.setColor(ChatColor.RED);

    }

    @Override
    public void execute(CommandSender sender, String[] args) {

        if (args.length == 0) {

            sender.sendMessage(help);
            sender.sendMessage(usage);

        } else {

            ProxyServer.getInstance().getScheduler().runAsync(main, () -> {

                try {

                    UUID u = fetcher.fetchUUID(args[0]);

                    if (u == null) {

                        sender.sendMessage(noplayer);

                    } else {

                        if (db.getBan(u) == null) {

                            TextComponent noban = new TextComponent(args[0]);

                            TextComponent extra = new TextComponent(" is not banned.");
                            extra.setColor(ChatColor.RED);
                            noban.addExtra(extra);

                            sender.sendMessage(noban);

                        } else {

                            db.unban(u, sender.getName());

                            TextComponent notification = new TextComponent(sender.getName());

                            TextComponent unbanned = new TextComponent(" unbanned ");
                            unbanned.setColor(ChatColor.RED);
                            notification.addExtra(unbanned);

                            TextComponent name = new TextComponent(args[0]);
                            name.setColor(ChatColor.WHITE);
                            notification.addExtra(name);

                            for (ProxiedPlayer player : ProxyServer.getInstance().getPlayers()) {

                                if (player.hasPermission("kxstaffsuite.notify.unban")) {

                                    player.sendMessage(notification);

                                }

                            }

                            ProxyServer.getInstance().getConsole().sendMessage(notification);

                            if (!sender.hasPermission("kxstaffsuite.notify.unban")) {

                                TextComponent noperm = new TextComponent("Unbanned ");
                                noperm.setColor(ChatColor.RED);

                                noperm.addExtra(name);

                                sender.sendMessage(noperm);

                            }

                        }

                    }

                } catch (DatabaseException e) {

                    sender.sendMessage(dberror);
                    e.printStackTrace();

                } catch (IOException e) {

                    sender.sendMessage(uuiderror);
                    e.printStackTrace();

                }

            });

        }

    }

}
