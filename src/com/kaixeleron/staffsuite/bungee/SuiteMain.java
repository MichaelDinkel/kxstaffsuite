package com.kaixeleron.staffsuite.bungee;

import com.google.common.io.ByteStreams;
import com.kaixeleron.staffsuite.bungee.command.*;
import com.kaixeleron.staffsuite.bungee.database.Database;
import com.kaixeleron.staffsuite.bungee.database.DatabaseException;
import com.kaixeleron.staffsuite.bungee.database.SQLDatabase;
import com.kaixeleron.staffsuite.bungee.database.YAMLDatabase;
import com.kaixeleron.staffsuite.bungee.fetcher.UUIDFetcher;
import com.kaixeleron.staffsuite.bungee.listener.ChatListener;
import com.kaixeleron.staffsuite.bungee.listener.LoginListener;
import com.kaixeleron.staffsuite.time.TimeManager;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.*;
import java.sql.SQLException;

public class SuiteMain extends Plugin {

    private Database database = null;

    @SuppressWarnings({"ResultOfMethodCallIgnored", "UnstableApiUsage"})
    @Override
    public void onEnable() {

        if (!getDataFolder().exists()) getDataFolder().mkdir();

        File configFile = new File(getDataFolder(), "config.yml");

        Configuration config;

        try {

            if (!configFile.exists()) {

                configFile.createNewFile();

                try (InputStream is = getResourceAsStream("config.yml"); OutputStream os = new FileOutputStream(configFile)) {

                     ByteStreams.copy(is, os);

                }

            }

            config = ConfigurationProvider.getProvider(YamlConfiguration.class).load(configFile);

        } catch (IOException e) {

            System.err.println("Could not access config.yml");
            e.printStackTrace();
            return;

        }

        if (config.getBoolean("sql.enabled")) {

            try {

                Configuration c = config.getSection("sql");

                database = new SQLDatabase(this, c.getString("host"), c.getInt("port"), c.getString("database"), c.getString("username"), c.getString("password"));

            } catch (SQLException e) {

                System.err.println("Could not connect to the MySQL database.");
                e.printStackTrace();

            }

        } else {

            try {

                database = new YAMLDatabase(this);

            } catch (DatabaseException e) {

                System.err.println("Could not set up the YAML database.");
                e.printStackTrace();

            }

        }

        TimeManager tm = new TimeManager();

        UUIDFetcher fetcher = new UUIDFetcher(this);

        ChatListener chatListener = new ChatListener(database, tm);

        getProxy().getPluginManager().registerCommand(this, new CommandKick());
        getProxy().getPluginManager().registerCommand(this, new CommandSilence(chatListener));

        if (database != null) {

            getProxy().getPluginManager().registerCommand(this, new CommandTempmute(this, database, tm, fetcher));
            getProxy().getPluginManager().registerCommand(this, new CommandTempban(this, database, tm, fetcher));
            getProxy().getPluginManager().registerCommand(this, new CommandMute(this, database, fetcher));
            getProxy().getPluginManager().registerCommand(this, new CommandBan(this, database, fetcher));
            getProxy().getPluginManager().registerCommand(this, new CommandUnmute(this, database, fetcher));
            getProxy().getPluginManager().registerCommand(this, new CommandUnban(this, database, fetcher));
            getProxy().getPluginManager().registerCommand(this, new CommandCheck(this, database, tm, fetcher));
            getProxy().getPluginManager().registerCommand(this, new CommandBanHistory(this, database, tm, fetcher));
            getProxy().getPluginManager().registerCommand(this, new CommandMuteHistory(this, database, tm, fetcher));
            getProxy().getPluginManager().registerCommand(this, new CommandAlts(this, database, fetcher));
            getProxy().getPluginManager().registerCommand(this, new CommandPoison(database));
            getProxy().getPluginManager().registerCommand(this, new CommandUnpoison(database));
            getProxy().getPluginManager().registerCommand(this, new CommandCheckPoison(database, tm));

        }

        getProxy().getPluginManager().registerListener(this, new LoginListener(database, tm, fetcher));
        getProxy().getPluginManager().registerListener(this, chatListener);

        for (String s : config.getSection("staffChats").getKeys()) {

            getProxy().getPluginManager().registerCommand(this, new CommandStaffChat(ChatColor.getByChar(config.getString(String.format("staffChats.%s.color", s)).toCharArray()[0]), s));

        }

    }

    @Override
    public void onDisable() {

        try {

            database.close();

        } catch (DatabaseException e) {

            e.printStackTrace();

        }

    }


}
