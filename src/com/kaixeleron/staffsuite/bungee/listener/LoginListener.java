package com.kaixeleron.staffsuite.bungee.listener;

import com.kaixeleron.staffsuite.bungee.database.Database;
import com.kaixeleron.staffsuite.bungee.database.DatabaseException;
import com.kaixeleron.staffsuite.bungee.database.data.Ban;
import com.kaixeleron.staffsuite.bungee.database.data.Poison;
import com.kaixeleron.staffsuite.bungee.fetcher.UUIDFetcher;
import com.kaixeleron.staffsuite.bungee.permission.WildcardHandler;
import com.kaixeleron.staffsuite.time.TimeManager;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.event.LoginEvent;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class LoginListener implements Listener {

    private final Database db;

    private final TimeManager timeManager;

    private final UUIDFetcher fetcher;

    private final WildcardHandler handler;

    private final TextComponent error;

    public LoginListener(Database db, TimeManager timeManager, UUIDFetcher fetcher) {

        this.db = db;

        this.timeManager = timeManager;

        this.fetcher = fetcher;

        handler = new WildcardHandler();

        error = new TextComponent("A database error has occurred while logging in. This does not affect any ban status you may have.");

    }

    @EventHandler
    public void onLogin(LoginEvent event) {

        fetcher.cache(event.getConnection().getName(), event.getConnection().getUniqueId());

        String ip = event.getConnection().getAddress().getAddress().toString().substring(1);

        if (ip.contains(":")) ip = ip.split("[%]")[0]; //IPv6

        try {

            db.updateAlt(ip, event.getConnection().getUniqueId(), event.getConnection().getName());

            Poison poison = db.getPoison(ip);

            Ban ban = db.getBan(event.getConnection().getUniqueId());

            if (poison != null && ban == null) {

                db.ban(event.getConnection().getUniqueId(), -1L, poison.getReason(), poison.getBy());

                TextComponent bantext = new TextComponent("Banned\n\nReason: ");
                bantext.setColor(ChatColor.RED);

                TextComponent reason = new TextComponent(poison.getReason());
                reason.setColor(ChatColor.WHITE);

                bantext.addExtra(reason);

                event.setCancelReason(bantext);
                event.setCancelled(true);

            } else {

                if (ban != null) {

                    long expiry = ban.getExpiry();

                    if (expiry == -1) {

                        TextComponent bantext = new TextComponent("Banned\n\nReason: ");
                        bantext.setColor(ChatColor.RED);

                        TextComponent reason = new TextComponent(ban.getReason());
                        reason.setColor(ChatColor.WHITE);

                        bantext.addExtra(reason);

                        event.setCancelReason(bantext);
                        event.setCancelled(true);

                    } else {

                        TextComponent bantext = new TextComponent("Temporarily banned for ");
                        bantext.setColor(ChatColor.RED);

                        TextComponent time = new TextComponent(timeManager.timeToString(expiry - System.currentTimeMillis()));
                        time.setColor(ChatColor.WHITE);

                        TextComponent colon = new TextComponent("\n\nReason: ");
                        colon.setColor(ChatColor.RED);

                        TextComponent reason = new TextComponent(ban.getReason());
                        reason.setColor(ChatColor.WHITE);

                        bantext.addExtra(time);
                        bantext.addExtra(colon);
                        bantext.addExtra(reason);

                        event.setCancelReason(bantext);
                        event.setCancelled(true);

                    }

                }

            }

        } catch (DatabaseException e) {

            event.setCancelReason(error);
            event.setCancelled(true);
            e.printStackTrace();

        }

    }

    @EventHandler
    public void onPostLogin(PostLoginEvent event) {

        handler.handleWildcards(event.getPlayer());

    }

}
