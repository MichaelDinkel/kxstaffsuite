package com.kaixeleron.staffsuite.bungee.listener;

import com.kaixeleron.staffsuite.bungee.database.Database;
import com.kaixeleron.staffsuite.bungee.database.DatabaseException;
import com.kaixeleron.staffsuite.bungee.database.data.Mute;
import com.kaixeleron.staffsuite.time.TimeManager;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class ChatListener implements Listener {

    private final Database db;

    private final TimeManager timeManager;

    private volatile boolean silenced;

    private final TextComponent silencedText, permMute, tempMute;

    public ChatListener(Database db, TimeManager timeManager) {

        this.db = db;

        this.timeManager = timeManager;

        silencedText = new TextComponent("The server is currently silenced.");
        silencedText.setColor(ChatColor.RED);

        permMute = new TextComponent("You are permanently muted.");
        permMute.setColor(ChatColor.RED);

        tempMute = new TextComponent("You are temporarily muted.");
        tempMute.setColor(ChatColor.RED);

    }

    @EventHandler
    public void onChat(ChatEvent event) {

        if (event.getSender() instanceof ProxiedPlayer) {

            ProxiedPlayer p = (ProxiedPlayer) event.getSender();

            if (silenced) {

                if (!p.hasPermission("kxstaffsuite.command.silence")) {

                    event.setCancelled(true);
                    p.sendMessage(silencedText);

                }

            } else {

                try {

                    Mute mute = db.getMute(p.getUniqueId());

                    if (mute != null) {

                        event.setCancelled(true);

                        long expiry = mute.getExpiry();

                        if (expiry == -1) {

                            TextComponent reason = new TextComponent("Reason: ");
                            reason.setColor(ChatColor.RED);

                            TextComponent reasonText = new TextComponent(mute.getReason());
                            reasonText.setColor(ChatColor.WHITE);

                            reason.addExtra(reasonText);

                            p.sendMessage(permMute);
                            p.sendMessage(reason);

                        } else {

                            TextComponent duration = new TextComponent("Duration: ");
                            duration.setColor(ChatColor.RED);

                            TextComponent durationText = new TextComponent(timeManager.timeToString(expiry - System.currentTimeMillis()));
                            durationText.setColor(ChatColor.WHITE);

                            duration.addExtra(durationText);


                            TextComponent reason = new TextComponent("Reason: ");
                            reason.setColor(ChatColor.RED);

                            TextComponent reasonText = new TextComponent(mute.getReason());
                            reasonText.setColor(ChatColor.WHITE);

                            reason.addExtra(reasonText);

                            p.sendMessage(tempMute);
                            p.sendMessage(duration);
                            p.sendMessage(reason);

                        }

                    }

                } catch (DatabaseException e) {

                    e.printStackTrace();

                }

            }

        }

    }

    public void setSilenced(boolean silenced) {

        this.silenced = silenced;

    }

    public boolean isSilenced() {

        return silenced;

    }
}
