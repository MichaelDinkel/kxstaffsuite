package com.kaixeleron.staffsuite.bungee.permission;

import net.md_5.bungee.api.connection.ProxiedPlayer;

public class WildcardHandler {

    public void handleWildcards(ProxiedPlayer p) {

        if (p.hasPermission("kxstaffsuite.*")) {

            p.setPermission("kxstaffsuite.command.*", true);
            p.setPermission("kxstaffsuite.notify.*", true);

        }

        if (p.hasPermission("kxstaffsuite.command.*")) {

            p.setPermission("kxstaffsuite.command.kick", true);
            p.setPermission("kxstaffsuite.command.tempmute", true);
            p.setPermission("kxstaffsuite.command.mute", true);
            p.setPermission("kxstaffsuite.command.tempban", true);
            p.setPermission("kxstaffsuite.command.ban", true);
            p.setPermission("kxstaffsuite.command.unmute", true);
            p.setPermission("kxstaffsuite.command.unban", true);
            p.setPermission("kxstaffsuite.command.check", true);
            p.setPermission("kxstaffsuite.command.staffchat.a", true);
            p.setPermission("kxstaffsuite.command.staffchat.q", true);
            p.setPermission("kxstaffsuite.command.silence", true);
            p.setPermission("kxstaffsuite.command.banhistory", true);
            p.setPermission("kxstaffsuite.command.mutehistory", true);
            p.setPermission("kxstaffsuite.command.alts", true);
            p.setPermission("kxstaffsuite.command.alts.ip", true);
            p.setPermission("kxstaffsuite.command.poison", true);
            p.setPermission("kxstaffsuite.command.unpoison", true);
            p.setPermission("kxstaffsuite.command.checkpoison", true);

        }

        if (p.hasPermission("kxstaffsuite.notify.*")) {

            p.setPermission("kxstaffsuite.notify.kick", true);
            p.setPermission("kxstaffsuite.notify.tempmute", true);
            p.setPermission("kxstaffsuite.notify.mute", true);
            p.setPermission("kxstaffsuite.notify.tempban", true);
            p.setPermission("kxstaffsuite.notify.ban", true);
            p.setPermission("kxstaffsuite.notify.unmute", true);
            p.setPermission("kxstaffsuite.notify.unban", true);
            p.setPermission("kxstaffsuite.notify.poison", true);
            p.setPermission("kxstaffsuite.notify.unpoison", true);

        }

    }
}
