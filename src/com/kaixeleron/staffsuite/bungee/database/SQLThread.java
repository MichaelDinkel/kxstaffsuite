package com.kaixeleron.staffsuite.bungee.database;

import com.kaixeleron.staffsuite.bungee.database.data.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class SQLThread implements Runnable {

    private final String hostname, database, username, password;

    private final int port;

    private Connection c;

    private final List<Ban> banQueue, unbanQueue;
    private final List<Mute> muteQueue, unmuteQueue;
    private final List<Alt> altQueue;
    private final List<Poison> poisonQueue, unpoisonQueue;

    private boolean running = true;

    SQLThread(String hostname, int port, String database, String username, String password) {

        this.hostname = hostname;
        this.port = port;
        this.database = database;
        this.username = username;
        this.password = password;

        banQueue = Collections.synchronizedList(new ArrayList<>());
        muteQueue = Collections.synchronizedList(new ArrayList<>());
        unbanQueue = Collections.synchronizedList(new ArrayList<>());
        unmuteQueue = Collections.synchronizedList(new ArrayList<>());
        altQueue = Collections.synchronizedList(new ArrayList<>());
        poisonQueue = Collections.synchronizedList(new ArrayList<>());
        unpoisonQueue = Collections.synchronizedList(new ArrayList<>());

    }

    private void connect() throws SQLException {

        if (c == null || c.isClosed()) c = DriverManager.getConnection(String.format("jdbc:mysql://%s:%d/%s", hostname, port, database), username, password);

    }

    private void closeSilently(AutoCloseable... ac) {

        for (AutoCloseable a : ac) {

            try {

                a.close();

            } catch (Exception ignored) {}

        }

    }

    @Override
    public void run() {

        try {

            connect();

        } catch (SQLException e) {

            e.printStackTrace();

        }

        while (running) {

            synchronized (unbanQueue) {

                Iterator<Ban> unbanIterator = unbanQueue.iterator();

                while (unbanIterator.hasNext()) {

                    Ban b = unbanIterator.next();

                    PreparedStatement unban = null;

                    try {

                        connect();

                        unban = c.prepareStatement("UPDATE `kxstaffsuite_bans` SET `expired` = ?, `expiredby` = ? WHERE `uuid` = ?;");
                        unban.setBoolean(1, true);
                        unban.setString(2,  b.getBy());
                        unban.setString(3, b.getUserId().toString());

                        unban.executeUpdate();

                    } catch (SQLException e) {

                        e.printStackTrace();

                    } finally {

                        closeSilently(unban);

                    }

                    unbanIterator.remove();

                }

            }

            synchronized (unmuteQueue) {

                Iterator<Mute> unmuteIterator = unmuteQueue.iterator();

                while (unmuteIterator.hasNext()) {

                    Mute m = unmuteIterator.next();

                    PreparedStatement unmute = null;

                    try {

                        connect();

                        unmute = c.prepareStatement("UPDATE `kxstaffsuite_mutes` SET `expired` = ?, `expiredby` = ? WHERE `uuid` = ?;");
                        unmute.setBoolean(1, true);
                        unmute.setString(2,  m.getBy());
                        unmute.setString(3, m.getUserId().toString());

                        unmute.executeUpdate();

                    } catch (SQLException e) {

                        e.printStackTrace();

                    } finally {

                        closeSilently(unmute);

                    }

                    unmuteIterator.remove();

                }

            }

            synchronized (unpoisonQueue) {

                Iterator<Poison> unpoisonIterator = unpoisonQueue.iterator();

                while (unpoisonIterator.hasNext()) {

                    Poison p = unpoisonIterator.next();

                    PreparedStatement poison = null;

                    try {

                        connect();

                        poison = c.prepareStatement("UPDATE `kxstaffsuite_poison` SET `removed` = ?, `removedby` = ? WHERE `ip` = ?;");
                        poison.setBoolean(1, true);
                        poison.setString(2,  p.getBy());
                        poison.setString(3, p.getIp());

                        poison.executeUpdate();

                    } catch (SQLException e) {

                        e.printStackTrace();

                    } finally {

                        closeSilently(poison);

                    }

                    unpoisonIterator.remove();

                }

            }

            synchronized (banQueue) {

                Iterator<Ban> banIterator = banQueue.iterator();

                while (banIterator.hasNext()) {

                    Ban b = banIterator.next();

                    PreparedStatement ban = null;

                    try {

                        connect();

                        ban = c.prepareStatement("INSERT INTO `kxstaffsuite_bans` (`uuid`,`expiry`,`reason`,`bannedby`,`expired`,`expiredby`,`placed`) VALUES (?, ?, ?, ?, ?, ?,?);");
                        ban.setString(1, b.getUserId().toString());
                        ban.setLong(2, b.getExpiry());
                        ban.setString(3, b.getReason());
                        ban.setString(4, b.getBy());
                        ban.setBoolean(5, false);
                        ban.setString(6, null);
                        ban.setLong(7, System.currentTimeMillis());

                        ban.executeUpdate();

                    } catch (SQLException e) {

                        e.printStackTrace();

                    } finally {

                        closeSilently(ban);

                    }

                    banIterator.remove();

                }

            }

            synchronized (muteQueue) {

                Iterator<Mute> muteIterator = muteQueue.iterator();

                while (muteIterator.hasNext()) {

                    Mute m = muteIterator.next();

                    PreparedStatement mute = null;

                    try {

                        connect();

                        mute = c.prepareStatement("INSERT INTO `kxstaffsuite_mutes` (`uuid`,`expiry`,`reason`,`mutedby`,`expired`,`expiredby`,`placed`) VALUES (?, ?, ?, ?, ?, ?, ?);");
                        mute.setString(1, m.getUserId().toString());
                        mute.setLong(2, m.getExpiry());
                        mute.setString(3, m.getReason());
                        mute.setString(4, m.getBy());
                        mute.setBoolean(5, false);
                        mute.setString(6, null);
                        mute.setLong(7, System.currentTimeMillis());

                        mute.executeUpdate();

                    } catch (SQLException e) {

                        e.printStackTrace();

                    } finally {

                        closeSilently(mute);

                    }

                    muteIterator.remove();

                }

            }

            synchronized (altQueue) {

                Iterator<Alt> altIterator = altQueue.iterator();

                while (altIterator.hasNext()) {

                    Alt a = altIterator.next();

                    PreparedStatement alt = null;

                    try {

                        connect();

                        alt = c.prepareStatement("INSERT INTO `kxstaffsuite_alts` (`ip`, `uuid`, `name`, `lastlogin`) VALUES (?, ?, ?, ?) ON DUPLICATE KEY UPDATE `ip` = ?, `name` = ?;");
                        alt.setString(1, a.getIp());
                        alt.setString(2, a.getId().toString());
                        alt.setString(3, a.getName());
                        alt.setLong(4, a.getLastLogin());
                        alt.setString(5, a.getIp());
                        alt.setString(6, a.getName());

                        alt.executeUpdate();

                    } catch (SQLException e) {

                        e.printStackTrace();

                    } finally {

                        closeSilently(alt);

                    }

                    altIterator.remove();

                }

            }

            synchronized (poisonQueue) {

                Iterator<Poison> poisonIterator = poisonQueue.iterator();

                while (poisonIterator.hasNext()) {

                    Poison p = poisonIterator.next();

                    PreparedStatement poison = null;

                    try {

                        connect();

                        poison = c.prepareStatement("INSERT INTO `kxstaffsuite_poison` (`ip`,`reason`,`poisonedby`,`removed`,`removedby`,`placed`) VALUES (?, ?, ?, ?, ?, ?);");
                        poison.setString(1, p.getIp());
                        poison.setString(2, p.getReason());
                        poison.setString(3, p.getBy());
                        poison.setBoolean(4, false);
                        poison.setString(5, null);
                        poison.setLong(6, System.currentTimeMillis());

                        poison.executeUpdate();

                    } catch (SQLException e) {

                        e.printStackTrace();

                    } finally {

                        closeSilently(poison);

                    }

                    poisonIterator.remove();

                }

            }

            try {

                Thread.sleep(1000);

            } catch (InterruptedException ignored) {}

        }

    }

    public void end() {

        running = false;

    }

    void addBan(Ban b) {

        synchronized (banQueue) {

            banQueue.add(b);

        }

    }

    void addMute(Mute m) {

        synchronized (muteQueue) {

            muteQueue.add(m);

        }

    }

    void addAlt(Alt a) {

        synchronized (altQueue) {

            altQueue.add(a);

        }

    }

    void addPoison(Poison p) {

        synchronized (poisonQueue) {

            poisonQueue.add(p);

        }

    }

    void removeBan(Ban b) {

        boolean contained = false;

        synchronized (banQueue) {

            if (banQueue.contains(b)) {

                banQueue.remove(b);
                contained = true;

            }

        }

        if (!contained) {

            synchronized (unbanQueue) {

                unbanQueue.add(b);

            }

        }

    }

    void removeMute(Mute m) {

        boolean contained = false;

        synchronized (muteQueue) {

            if (muteQueue.contains(m)) {

                muteQueue.remove(m);
                contained = true;

            }

        }

        if (!contained) {

            synchronized (unmuteQueue) {

                unmuteQueue.add(m);

            }

        }

    }

    void removePoison(Poison p) {

        boolean contained = false;

        synchronized (poisonQueue) {

            if (poisonQueue.contains(p)) {

                poisonQueue.remove(p);
                contained = true;

            }

        }

        if (!contained) {

            synchronized (unpoisonQueue) {

                unpoisonQueue.add(p);

            }

        }

    }
}
