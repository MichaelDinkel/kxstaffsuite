package com.kaixeleron.staffsuite.bungee.database;

import com.kaixeleron.staffsuite.bungee.SuiteMain;
import com.kaixeleron.staffsuite.bungee.database.data.Alt;
import com.kaixeleron.staffsuite.bungee.database.data.Ban;
import com.kaixeleron.staffsuite.bungee.database.data.Mute;
import com.kaixeleron.staffsuite.bungee.database.data.Poison;
import jline.internal.Nullable;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

public class YAMLDatabase implements Database {

    private final File bansFolder, mutesFolder, poisonedFolder, altsFile;

    private final Configuration alts;

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public YAMLDatabase(SuiteMain m) throws DatabaseException {

        m.getDataFolder().mkdir();

        bansFolder = new File(m.getDataFolder(), "bans");
        mutesFolder = new File(m.getDataFolder(), "mutes");
        poisonedFolder = new File(m.getDataFolder(), "poisoned");
        altsFile = new File(m.getDataFolder(), "alts.yml");

        bansFolder.mkdir();
        mutesFolder.mkdir();
        poisonedFolder.mkdir();

        if (!altsFile.exists()) {

            try {

                altsFile.createNewFile();

            } catch (IOException e) {

                throw new DatabaseException(e);

            }

        }

        try {

            alts = ConfigurationProvider.getProvider(YamlConfiguration.class).load(altsFile);

        } catch (IOException e) {

            throw new DatabaseException(e);

        }

    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    @Override
    public void ban(UUID u, long expiry, String reason, String by) throws DatabaseException {

        File banFile = new File(bansFolder, u.toString() + ".yml");

        if (!banFile.exists()) {

            try {

                banFile.createNewFile();

            } catch (IOException e) {

                throw new DatabaseException(e);

            }

        }

        try {

            Configuration ban = ConfigurationProvider.getProvider(YamlConfiguration.class).load(banFile);

            ban.set("banned", true);
            ban.set("expiry", expiry);
            ban.set("reason", reason);
            ban.set("by", by);
            ban.set("placed", System.currentTimeMillis());

            ConfigurationProvider.getProvider(YamlConfiguration.class).save(ban, banFile);

        } catch (IOException e) {

            throw new DatabaseException(e);

        }

    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    @Override
    public void mute(UUID u, long expiry, String reason, String by) throws DatabaseException {

        File muteFile = new File(mutesFolder, u.toString() + ".yml");

        if (!muteFile.exists()) {

            try {

                muteFile.createNewFile();

            } catch (IOException e) {

                throw new DatabaseException(e);

            }

        }

        try {

            Configuration mute = ConfigurationProvider.getProvider(YamlConfiguration.class).load(muteFile);

            mute.set("banned", true);
            mute.set("expiry", expiry);
            mute.set("reason", reason);
            mute.set("by", by);
            mute.set("placed", System.currentTimeMillis());

            ConfigurationProvider.getProvider(YamlConfiguration.class).save(mute, muteFile);

        } catch (IOException e) {

            throw new DatabaseException(e);

        }

    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    @Override
    public void poison(String ip, String reason, String by) throws DatabaseException {

        File poisonFile = new File(poisonedFolder, ip + ".yml");

        if (!poisonFile.exists()) {

            try {

                poisonFile.createNewFile();

            } catch (IOException e) {

                throw new DatabaseException(e);

            }

        }

        try {

            Configuration poison = ConfigurationProvider.getProvider(YamlConfiguration.class).load(poisonFile);

            poison.set("poisoned", true);
            poison.set("reason", reason);
            poison.set("by", by);
            poison.set("placed", System.currentTimeMillis());

            ConfigurationProvider.getProvider(YamlConfiguration.class).save(poison, poisonFile);

        } catch (IOException e) {

            throw new DatabaseException(e);

        }

    }

    @Override
    public void unban(UUID u, String by) throws DatabaseException {

        File banFile = new File(bansFolder, u.toString() + ".yml");

        if (banFile.exists()) {

            try {

                Configuration ban = ConfigurationProvider.getProvider(YamlConfiguration.class).load(banFile);

                if (ban.getBoolean("banned", false)) {

                    int current = 0;

                    Configuration pastBans = ban.getSection("pastBans");

                    if (pastBans != null) current = pastBans.getKeys().size();

                    ban.set(String.format("pastBans.%d.expiry", current), ban.get("expiry"));
                    ban.set(String.format("pastBans.%d.reason", current), ban.get("reason"));
                    ban.set(String.format("pastBans.%d.by", current), ban.get("by"));
                    ban.set(String.format("pastBans.%d.placed", current), ban.get("placed"));
                    ban.set(String.format("pastBans.%d.removedBy", current), by);

                    ban.set("banned", null);
                    ban.set("expiry", null);
                    ban.set("reason", null);
                    ban.set("by", null);
                    ban.set("placed", null);

                    ConfigurationProvider.getProvider(YamlConfiguration.class).save(ban, banFile);

                }

            } catch (IOException e) {

                throw new DatabaseException(e);

            }

        }

    }

    @Override
    public void unmute(UUID u, String by) throws DatabaseException {

        File muteFile = new File(mutesFolder, u.toString() + ".yml");

        if (muteFile.exists()) {

            try {

                Configuration mute = ConfigurationProvider.getProvider(YamlConfiguration.class).load(muteFile);

                if (mute.getBoolean("muted", false)) {

                    int current = 0;

                    Configuration pastMutes = mute.getSection("pastMutes");

                    if (pastMutes != null) current = pastMutes.getKeys().size();

                    mute.set(String.format("pastMutes.%d.expiry", current), mute.get("expiry"));
                    mute.set(String.format("pastMutes.%d.reason", current), mute.get("reason"));
                    mute.set(String.format("pastMutes.%d.by", current), mute.get("by"));
                    mute.set(String.format("pastMutes.%d.placed", current), mute.get("placed"));
                    mute.set(String.format("pastMutes.%d.removedBy", current), by);

                    mute.set("banned", null);
                    mute.set("expiry", null);
                    mute.set("reason", null);
                    mute.set("by", null);
                    mute.set("placed", null);

                    ConfigurationProvider.getProvider(YamlConfiguration.class).save(mute, muteFile);

                }

            } catch (IOException e) {

                throw new DatabaseException(e);

            }

        }

    }

    @Override
    public void unpoison(String ip, String by) throws DatabaseException {

        File poisonFile = new File(poisonedFolder, ip + ".yml");

        if (poisonFile.exists()) {

            try {

                Configuration poison = ConfigurationProvider.getProvider(YamlConfiguration.class).load(poisonFile);

                if (poison.getBoolean("poisoned", false)) {

                    Configuration pastPoisons = poison.getSection("pastPoisons");

                    int current = pastPoisons.getKeys().size();

                    pastPoisons.set(String.format("%d.reason", current), poison.get("reason"));
                    pastPoisons.set(String.format("%d.by", current), poison.get("by"));
                    pastPoisons.set(String.format("%d.placed", current), poison.get("placed"));
                    pastPoisons.set(String.format("%d.removedBy", current), by);

                    poison.set("poisoned", null);
                    poison.set("reason", null);
                    poison.set("by", null);
                    poison.set("placed", null);

                }

                ConfigurationProvider.getProvider(YamlConfiguration.class).save(poison, poisonFile);

            } catch (IOException e) {

                throw new DatabaseException(e);

            }

        }

    }

    @Override
    public void updateAlt(String ip, UUID uuid, String name) throws DatabaseException {

        //TODO

    }

    @Override
    public @Nullable Ban getBan(UUID u) throws DatabaseException {

        File banFile = new File(bansFolder, u.toString() + ".yml");

        if (banFile.exists()) {

            try {

                Configuration ban = ConfigurationProvider.getProvider(YamlConfiguration.class).load(banFile);

                if (ban.getBoolean("banned", false)) {

                    return new Ban(u, ban.getString("reason"), ban.getLong("expiry"), ban.getString("by"), false, "", ban.getLong("placed"));

                }

            } catch (IOException e) {

                throw new DatabaseException(e);

            }

        }

        return null;

    }

    @Override
    public @Nullable Mute getMute(UUID u) throws DatabaseException {

        File muteFile = new File(mutesFolder, u.toString() + ".yml");

        if (muteFile.exists()) {

            try {

                Configuration mute = ConfigurationProvider.getProvider(YamlConfiguration.class).load(muteFile);

                if (mute.getBoolean("muted", false)) {

                    return new Mute(u, mute.getString("reason"), mute.getLong("expiry"), mute.getString("by"), false, "", mute.getLong("placed"));

                }

            } catch (IOException e) {

                throw new DatabaseException(e);

            }

        }

        return null;

    }

    @Override
    public @Nullable Poison getPoison(String ip) throws DatabaseException {

        File poisonFile = new File(poisonedFolder, ip + ".yml");

        if (poisonFile.exists()) {

            try {

                Configuration poison = ConfigurationProvider.getProvider(YamlConfiguration.class).load(poisonFile);

                if (poison.getBoolean("poisoned", false)) {

                    return new Poison(ip, poison.getString("reason"), poison.getString("by"), false, "", poison.getLong("placed"));

                }

            } catch (IOException e) {

                throw new DatabaseException(e);

            }

        }

        return null;

    }

    @Override
    public Ban[] getBanHistory(UUID u) throws DatabaseException {

        File banFile = new File(bansFolder, u.toString() + ".yml");

        if (banFile.exists()) {

            try {

                Configuration bans = ConfigurationProvider.getProvider(YamlConfiguration.class).load(banFile).getSection("pastBans");

                if (bans != null) {

                    Ban[] history = new Ban[bans.getKeys().size()];

                    for (int i = 0; i < history.length; i++) {

                        history[i] = new Ban(u, bans.getString(String.format("%d.reason", i)), bans.getLong(String.format("%d.expiry", i)), bans.getString(String.format("%d.by", i)), true, bans.getString(String.format("%d.removedBy", i)), bans.getLong(String.format("%d.placed", i)));

                    }

                    return history;

                }

            } catch (IOException e) {

                throw new DatabaseException(e);

            }

        }

        return new Ban[0];

    }

    @Override
    public Mute[] getMuteHistory(UUID u) throws DatabaseException {

        File muteFile = new File(mutesFolder, u.toString() + ".yml");

        if (muteFile.exists()) {

            try {

                Configuration mutes = ConfigurationProvider.getProvider(YamlConfiguration.class).load(muteFile).getSection("pastMutes");

                if (mutes != null) {

                    Mute[] history = new Mute[mutes.getKeys().size()];

                    for (int i = 0; i < history.length; i++) {

                        history[i] = new Mute(u, mutes.getString(String.format("%d.reason", i)), mutes.getLong(String.format("%d.expiry", i)), mutes.getString(String.format("%d.by", i)), true, mutes.getString(String.format("%d.removedBy", i)), mutes.getLong(String.format("%d.placed", i)));

                    }

                    return history;

                }

            } catch (IOException e) {

                throw new DatabaseException(e);

            }

        }

        return new Mute[0];

    }

    //TODO
    @Override
    public Alt[] getAlts(String ip) {
        return new Alt[0];
    }

    //TODO
    @Override
    public Alt[] getAlts(UUID id) {
        return new Alt[0];
    }

    @Override
    public Poison[] getPoisonHistory(String ip) throws DatabaseException {

        File poisonFile = new File(poisonedFolder, ip + ".yml");

        if (poisonFile.exists()) {

            try {

                Configuration poisons = ConfigurationProvider.getProvider(YamlConfiguration.class).load(poisonFile).getSection("pastPoisons");

                if (poisons != null) {

                    Poison[] history = new Poison[poisons.getKeys().size()];

                    for (int i = 0; i < history.length; i++) {

                        history[i] = new Poison(ip, poisons.getString(String.format("%d.reason", i)), poisons.getString(String.format("%d.by", i)), true, poisons.getString(String.format("%d.removedBy", i)), poisons.getLong(String.format("%d.placed", i)));

                    }

                    return history;

                }

            } catch (IOException e) {

                throw new DatabaseException(e);

            }

        }

        return new Poison[0];

    }

    @Override
    public void close() {}

}
