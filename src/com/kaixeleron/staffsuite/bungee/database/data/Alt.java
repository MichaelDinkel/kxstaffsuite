package com.kaixeleron.staffsuite.bungee.database.data;

import java.util.UUID;

public class Alt {

    private final String ip, name;

    private final UUID id;

    private final long lastLogin;

    public Alt(String ip, UUID id, String name, long lastLogin) {

        this.ip = ip;
        this.id = id;
        this.name = name;
        this.lastLogin = lastLogin;

    }

    public String getIp() {

        return ip;

    }

    public UUID getId() {

        return id;

    }

    public String getName() {

        return name;

    }

    public long getLastLogin() {

        return lastLogin;

    }

}
