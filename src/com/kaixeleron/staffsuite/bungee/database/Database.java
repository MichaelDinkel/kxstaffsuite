package com.kaixeleron.staffsuite.bungee.database;

import com.kaixeleron.staffsuite.bungee.database.data.*;

import javax.annotation.Nullable;
import java.util.UUID;

public interface Database {

    void ban(UUID u, long expiry, String reason, String by) throws DatabaseException;

    void mute(UUID u, long expiry, String reason, String by) throws DatabaseException;

    void poison(String ip, String reason, String by) throws DatabaseException;

    void unban(UUID u, String by) throws DatabaseException;

    void unmute(UUID u, String by) throws DatabaseException;

    void unpoison(String ip, String by) throws DatabaseException;

    void updateAlt(String ip, UUID uuid, String name) throws DatabaseException;

    @Nullable Ban getBan(UUID u) throws DatabaseException;

    @Nullable Mute getMute(UUID u) throws DatabaseException;

    @Nullable Poison getPoison(String ip) throws DatabaseException;

    Ban[] getBanHistory(UUID u) throws DatabaseException;

    Mute[] getMuteHistory(UUID u) throws DatabaseException;

    Alt[] getAlts(String ip) throws DatabaseException;

    Alt[] getAlts(UUID id) throws DatabaseException;

    Poison[] getPoisonHistory(String ip) throws DatabaseException;

    void close() throws DatabaseException;

}
